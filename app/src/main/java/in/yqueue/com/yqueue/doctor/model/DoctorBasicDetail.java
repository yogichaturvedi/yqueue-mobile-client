package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh Chaturvedi on 23-04-2018.
 */

public class DoctorBasicDetail {
    private String id;
    private String name;
    private String specialization;
    private String clinic;
    private float experience;
    private int fees;
    private Address address;
    private String availability;

    public DoctorBasicDetail(@JsonProperty("id") String id,
                             @JsonProperty("searchResult") String name,
                             @JsonProperty("specialization") String specialization,
                             @JsonProperty("clinic") String clinic,
                             @JsonProperty("experience") float experience,
                             @JsonProperty("fees") int fees,
                             @JsonProperty("address") Address address,
                             @JsonProperty("availability") String availability) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
        this.clinic = clinic;
        this.experience = experience;
        this.fees = fees;
        this.address = address;
        this.availability = availability;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }

    public float getExperience() {
        return experience;
    }

    public void setExperience(float experience) {
        this.experience = experience;
    }

    public int getFees() {
        return fees;
    }

    public void setFees(int fees) {
        this.fees = fees;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }
}
