package in.yqueue.com.yqueue.doctor.search;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.doctor.DoctorListActivity;
import in.yqueue.com.yqueue.doctor.search.model.SearchModel;
import in.yqueue.com.yqueue.doctor.search.model.ServiceAndSpecializationDTO;
import in.yqueue.com.yqueue.service.Move;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class ServiceSpecializationSearchAdapter extends RecyclerView.Adapter<ServiceSpecializationSearchAdapter.ViewHolder> {
    private List<ServiceAndSpecializationDTO> clinicSearchList;

    public ServiceSpecializationSearchAdapter(List<ServiceAndSpecializationDTO> clinicSearchList) {
        this.clinicSearchList = clinicSearchList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ServiceSpecializationSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                            int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_specialization_search_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, clinicSearchList);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        ServiceAndSpecializationDTO doctorBasicDetail = clinicSearchList.get(position);
        viewHolder.name.setText(doctorBasicDetail.getName());
        viewHolder.type.setText(doctorBasicDetail.getType());
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return clinicSearchList.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final List<ServiceAndSpecializationDTO> serviceAndSpecializationDTOS;
        public TextView name;
        public TextView type;

        public ViewHolder(View itemLayoutView, List<ServiceAndSpecializationDTO> serviceAndSpecializationDTOS) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.service_name);
            type = (TextView) itemLayoutView.findViewById(R.id.service_type);
            this.serviceAndSpecializationDTOS = serviceAndSpecializationDTOS;
            itemLayoutView.setOnClickListener(this);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            SharedPreferences sharedPreferences = view.getContext().getSharedPreferences(view.getContext().getString(R.string.search_suggestion), Context.MODE_PRIVATE);
            String searchSuggestionString = sharedPreferences.getString(view.getContext().getString(R.string.search_suggestion), null);
            SearchModel searchModel;
            if (searchSuggestionString == null) {
                searchModel = new SearchModel();
            } else {
                try {
                    searchModel = new ObjectMapper().readValue(searchSuggestionString, SearchModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    searchModel = new SearchModel();
                }
            }
            try {
                ServiceAndSpecializationDTO itemToAdd = serviceAndSpecializationDTOS.get(getAdapterPosition());
                List<ServiceAndSpecializationDTO> serviceAndSpecializationResults = searchModel.getServiceAndSpecializationSearchResults();
                boolean isExists = serviceAndSpecializationResults.stream().anyMatch(e -> e.getName().equalsIgnoreCase(itemToAdd.getName()) && e.getType().equalsIgnoreCase(itemToAdd.getType()));
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if (isExists) {
                    serviceAndSpecializationResults.removeIf(e -> e.getType().equalsIgnoreCase(itemToAdd.getType()) && e.getName().equalsIgnoreCase(itemToAdd.getName()));
                    serviceAndSpecializationResults.add(itemToAdd);
                } else {
                    serviceAndSpecializationResults.add(itemToAdd);
                    if (serviceAndSpecializationResults.size() > 3) {
                        serviceAndSpecializationResults.remove(serviceAndSpecializationResults.get(0));
                    }
                }
                editor.putString(view.getContext().getString(R.string.search_suggestion), new ObjectMapper().writeValueAsString(searchModel));
                editor.apply();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bundle bundle = new Bundle();
            bundle.putString("type", serviceAndSpecializationDTOS.get(getAdapterPosition()).getType());
            bundle.putString("keyword", serviceAndSpecializationDTOS.get(getAdapterPosition()).getName());
            new Move().activity(view.getContext().getApplicationContext(), DoctorListActivity.class, bundle, "search");
        }
    }
}
