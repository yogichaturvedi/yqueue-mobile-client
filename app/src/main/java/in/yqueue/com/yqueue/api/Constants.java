package in.yqueue.com.yqueue.api;

/**
 * Created by Yogesh Chaturvedi on 21-04-2018.
 */

public class Constants {
    private static String API_URL = "http://54.186.111.254/yqueue/api/v1/";
    public static String API_PATIENT = API_URL + "patient/";
    public static String API_DOCTOR = API_URL + "doctor/";
    public static String API_SEARCH = API_DOCTOR + "search";
    public static String API_DOCTOR_BASIC = API_URL + "doctor/basic/";
    public static String API_DOCTOR_SEARCH = API_DOCTOR_BASIC + "search";
    public static String API_APPOINTMENT_BOOK = API_URL + "appointment/book";
    private static String API_IMAGE_URL = "http://54.186.111.254/yqueue-image-server/api/v1/";
    public static String API_IMAGE_DOCTOR = API_IMAGE_URL + "doctor/";
    public static String API_IMAGE_PATIENT = API_IMAGE_URL + "patient/";
    public static String GOOGLE_MAP_API = "https://maps.googleapis.com/maps/api/distancematrix/json";

    public static String Error_404 = "Something went wrong";

    public static String DOCTOR_SEARCH_SUGGESTION [] = {"Cardiologist","Dermatologist","Gastroenterologist","Gynecologist",
            "Neurologist","Ophthalmologist","Orthopedic","Ear Nose Throat Specialist","Psychiatrist","Urologist"};
    public static String TYPE_SPECIALIZATION = "specialization";
    public static String TYPE_CLINIC = "clinic";
    public static String TYPE_SERVICE = "service";
}
