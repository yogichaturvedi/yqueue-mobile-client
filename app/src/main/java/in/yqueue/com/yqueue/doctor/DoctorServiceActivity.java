package in.yqueue.com.yqueue.doctor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.doctor.model.Service;

public class DoctorServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_service_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Bundle servicesBundle = getIntent().getBundleExtra("services");
        if (servicesBundle != null) {
            toolbar.setTitle(servicesBundle.getString("doctorName"));
            toolbar.setSubtitle("Services");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.close);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView serviceList = findViewById(R.id.services_list);
        if (servicesBundle != null) {
            String servicesAsString = servicesBundle.getString("services");
            try {
                Service[] servicesArray = new ObjectMapper().readValue(servicesAsString, Service[].class);
                List<Service> services = Arrays.asList(servicesArray);
                serviceList.setLayoutManager(new LinearLayoutManager(DoctorServiceActivity.this, LinearLayoutManager.VERTICAL, false));
                DoctorServiceAdapter doctorServiceAdapter = new DoctorServiceAdapter(services, R.layout.doctor_service_list_item_big);
                serviceList.setAdapter(doctorServiceAdapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
