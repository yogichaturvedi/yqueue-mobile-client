package in.yqueue.com.yqueue.book;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.book.timeSlot.TimeSlotAdapter;
import in.yqueue.com.yqueue.book.timeSlot.model.TimeGroup;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.common.MySnackBar;
import in.yqueue.com.yqueue.user.User;
import in.yqueue.com.yqueue.util.Utils;

/**
 * Created by Naveen Kumawat on 16-04-2018.
 */

public class BookCustomDateFragment extends Fragment implements FragmentLifecycle {

    private DatePickerDialog datePickerDialog;
    private View view;
    private String doctorId, clinicId, dateISOString;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.book_custom_date_fragment, container, false);
        ExpandableListView expandableListView = view.findViewById(R.id.book_custom_date_expandable_list);
        RelativeLayout selectDateContainer = view.findViewById(R.id.select_date_container);
        Calendar c = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(
                getActivity(), new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                Date date = new Date(selectedYear, selectedMonth, selectedDay - 1);
                String dayOfWeek = simpledateformat.format(date);
                String dateToDisplay = selectedDay + " " + dayOfWeek.substring(0, 3);
                ((BookDateActivity) getActivity()).setCustomDate(dateToDisplay);
                datePickerDialog.cancel();
                selectDateContainer.setVisibility(View.GONE);
                expandableListView.setVisibility(View.VISIBLE);

                dateISOString = Utils.toISOString(date);

                fetchTimeSlots();
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 604800000);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
//                    ((BookDateActivity) getActivity()).setCustomDate("Select");
                    expandableListView.setVisibility(View.GONE);
                    selectDateContainer.setVisibility(View.VISIBLE);
                }
            }
        });

        view.findViewById(R.id.date_try_again).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (datePickerDialog != null && !datePickerDialog.isShowing()) {
                    datePickerDialog.show();
                }
            }
        });
        return view;
    }

    @Override
    public void onPauseFragment() {
        datePickerDialog.hide();
    }

    @Override
    public void onResumeFragment() {
        datePickerDialog.show();
    }

    private void fetchTimeSlots() {
        if (getArguments() != null) {
            doctorId = getArguments().getString("doctorId");
            clinicId = getArguments().getString("clinicId");
        }
        if (doctorId != null && clinicId != null && dateISOString != null) {
            ProgressDialog fetchTimeSlotProgressDialog = new MyProgressDialog().init(getActivity(), "", "", true);
            new FetchTimeSlots(fetchTimeSlotProgressDialog).execute();
        }
    }


    private void updateListView(ArrayList<TimeGroup> timeGroups) {
        ExpandableListView expandableListView = view.findViewById(R.id.book_custom_date_expandable_list);
        String user = getActivity().getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE).getString(String.valueOf(R.string.userSP_user_key), null);
        if (timeGroups.size() == 0) {
            expandableListView.setVisibility(View.GONE);
            view.findViewById(R.id.not_available_container).setVisibility(View.VISIBLE);
            return;
        } else {
            expandableListView.setVisibility(View.VISIBLE);
            view.findViewById(R.id.not_available_container).setVisibility(View.GONE);
        }

        if (user != null) {
            try {
                User user1 = new ObjectMapper().readValue(user, User.class);
                TimeSlotAdapter timeSlotAdapter = new TimeSlotAdapter(getContext(), timeGroups, user1.getId(), doctorId, clinicId);
                expandableListView.setAdapter(timeSlotAdapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class FetchTimeSlots extends AsyncTask<Void, Void, JSONObject> {
        String url = Constants.API_DOCTOR + doctorId + "/clinic/" + clinicId + "/timeSlots?date=" + dateISOString;
        ProgressDialog progressDialog;

        public FetchTimeSlots(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!statusCode.trim().equalsIgnoreCase("200")) {
                    Callable callable = new Callable() {
                        @Override
                        public Object call() throws Exception {
                            fetchTimeSlots();
                            return null;
                        }
                    };
                    new MySnackBar(view.findViewById(R.id.book_custom_date_expandable_list), Utils.getErrorMessage(statusCode), "indeterminant", Color.YELLOW, "RETRY", callable);
                    return;
                }
                try {
                    TimeGroup[] responses = (TimeGroup[]) response.get("response");
                    ArrayList<TimeGroup> timeGroups = new ArrayList<>(Arrays.asList(responses));
                    updateListView(timeGroups);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                TimeGroup[] timeGroups = RestService.instance().get(url, TimeGroup[].class);
                jsonObject.put("StatusCode", "200");
                jsonObject.put("response", timeGroups);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Error fetching doctor", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }
}
