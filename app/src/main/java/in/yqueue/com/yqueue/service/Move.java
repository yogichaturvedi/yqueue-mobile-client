package in.yqueue.com.yqueue.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by Yogesh Chaturvedi on 28-03-2018.
 */

public class Move {

    public void activity(Context context, Class aClass) {
        Intent intent = new Intent(context, aClass);
        context.startActivity(intent);
    }

    public void activityWithoutBackStack(Context context, Class aClass) {
        Intent intent = new Intent(context, aClass);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        context.startActivity(intent);
    }

    public void activity(Context context, Class aClass, Bundle bundle, String bundleName) {
        Intent intent = new Intent(context, aClass);
        if (bundle != null) {
            intent.putExtra(bundleName, bundle);
        }
        context.startActivity(intent);
    }

    public void activityWithoutBackStack(Context context, Class aClass, Bundle bundle, String bundleName) {
        Intent intent = new Intent(context, aClass);
        if (bundle != null) {
            intent.putExtra(bundleName, bundle);
        }
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        context.startActivity(intent);
    }

    public void fragment(FragmentManager fragmentManager, int container, Fragment destination, Bundle bundle) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (bundle != null) {
            fragmentManager.putFragment(bundle, "Frag", destination);
        } else {
            fragmentTransaction.add(container, destination, "Frag");
        }
        fragmentTransaction.addToBackStack("Frag");
        fragmentTransaction.commit();
    }
}
