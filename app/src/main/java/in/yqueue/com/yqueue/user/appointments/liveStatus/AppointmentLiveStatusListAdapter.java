package in.yqueue.com.yqueue.user.appointments.liveStatus;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.yqueue.com.yqueue.R;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class AppointmentLiveStatusListAdapter extends RecyclerView.Adapter<AppointmentLiveStatusListAdapter.ViewHolder> {
    private ArrayList<LiveStatusPojo> liveStatusList;

    public AppointmentLiveStatusListAdapter(ArrayList<LiveStatusPojo> liveStatusList) {

        this.liveStatusList = liveStatusList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AppointmentLiveStatusListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                          int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointment_live_status_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, liveStatusList);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        LiveStatusPojo liveStatus = liveStatusList.get(position);
        String statusToShow = liveStatus.getStatus();
        if (liveStatus.getTokenNumber() != 0) {
            statusToShow += " #" + liveStatus.getTokenNumber();
        }
        SpannableStringBuilder str = new SpannableStringBuilder(statusToShow);
        //str.setSpan(new android.text.style.StyleSpan(Typeface.BOLD), liveStatus.getStatus().length(), statusToShow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.setSpan(new ForegroundColorSpan(Color.parseColor("#3F51B5")), liveStatus.getStatus().length(), statusToShow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        viewHolder.time.setText(liveStatus.getTime());
        viewHolder.status.setText(str);
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView time;
        public TextView status;
        private final ArrayList<LiveStatusPojo> liveStatusList;


        public ViewHolder(View itemLayoutView, ArrayList<LiveStatusPojo> liveStatusList) {
            super(itemLayoutView);
            time = (TextView) itemLayoutView.findViewById(R.id.time);
            status = (TextView) itemLayoutView.findViewById(R.id.status);
            this.liveStatusList = liveStatusList;
        }

        @Override
        public void onClick(View view) {

        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return liveStatusList.size();
    }
}
