package in.yqueue.com.yqueue.user.appointments;

/**
 * Created by Naveen Kumawat on 18-04-2018.
 */

public class UserAppointmentPojo {

    private String doctorName;
    private String timeSlot;
    private String date;
    private String time;
    private String specialization;
    private String clinic;

    public UserAppointmentPojo(String doctorName, String timeSlot, String date, String time, String specialization, String clinic) {
        this.doctorName = doctorName;
        this.timeSlot = timeSlot;
        this.date = date;
        this.time = time;
        this.specialization = specialization;
        this.clinic = clinic;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }
}
