package in.yqueue.com.yqueue.book.timeSlot;

/**
 * Created by Naveen Kumawat on 16-04-2018.
 */

public class ChildInfo {

    private String timeSlot;
    private String status;
    private int filled;
    private int total;

    public ChildInfo(String timeSlot, String status, int filled, int total) {
        this.timeSlot = timeSlot;
        this.status = status;
        this.filled = filled;
        this.total = total;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getFilled() {
        return filled;
    }

    public void setFilled(int filled) {
        this.filled = filled;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
