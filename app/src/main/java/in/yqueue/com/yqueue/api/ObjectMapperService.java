package in.yqueue.com.yqueue.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by Yogesh on 21-07-2016.
 */
public class ObjectMapperService {
    private static ObjectMapperService _instance;
    private final ObjectMapper objectMapper;

    ObjectMapperService() {
        objectMapper = new ObjectMapper();
    }

    public static ObjectMapperService instance() {
        if (_instance == null) {
            _instance = new ObjectMapperService();
        }
        return _instance;
    }

    public <T> T readValue(String content, TypeReference valueTypeRef) {
        TypeReference typeReference;
        try {
            typeReference = objectMapper.readValue(content, valueTypeRef);
        } catch (IOException e) {
            typeReference = null;
            e.printStackTrace();
        }
        return (T) typeReference;

    }

    public <T> T readValue(String content, Class<T> valueType) {
        T t;
        try {
            t = objectMapper.readValue(content, valueType);
        } catch (IOException e) {
            t = null;
            e.printStackTrace();
        }
        return t;
    }

    public String writeValueAsString(Object value) throws JsonProcessingException {
        return objectMapper.writeValueAsString(value);
    }
}
