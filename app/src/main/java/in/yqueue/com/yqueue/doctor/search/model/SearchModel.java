package in.yqueue.com.yqueue.doctor.search.model;

import android.annotation.TargetApi;
import android.os.Build;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yogesh Chaturvedi on 05-06-2018.
 */

@TargetApi(Build.VERSION_CODES.M)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchModel implements Serializable {
    private List<DoctorSearchResultDTO> doctorSearchResults = new ArrayList<>();
    private List<ClinicSearchResultDTO> clinicSearchResults = new ArrayList<>();
    private List<ServiceAndSpecializationDTO> serviceAndSpecializationSearchResults = new ArrayList<>();
    /**
     * @return the doctorSearchResults
     */
    @JsonProperty("doctors")
    public List<DoctorSearchResultDTO> getDoctorSearchResults() {
        return doctorSearchResults;
    }
    /**
     * @param doctorSearchResults the doctorSearchResults to set
     */
    public void setDoctorSearchResults(List<DoctorSearchResultDTO> doctorSearchResults) {
        this.doctorSearchResults = doctorSearchResults;
    }
    /**
     * @return the clinicSearchResults
     */
    @JsonProperty("clinics")
    public List<ClinicSearchResultDTO> getClinicSearchResults() {
        return clinicSearchResults;
    }
    /**
     * @param clinicSearchResultDTOs the clinicSearchResults to set
     */
    public void setClinicSearchResults(List<ClinicSearchResultDTO> clinicSearchResultDTOs) {
        this.clinicSearchResults = clinicSearchResultDTOs;
    }
    /**
     * @return the serviceAndSpecializationSearchResults
     */
    @JsonProperty("serviceAndSpecialization")
    public List<ServiceAndSpecializationDTO> getServiceAndSpecializationSearchResults() {
        return serviceAndSpecializationSearchResults;
    }
    /**
     * @param serviceAndSpecializationSearchResults the serviceAndSpecializationSearchResults to set
     */
    public void setServiceAndSpecializationSearchResults(List<ServiceAndSpecializationDTO> serviceAndSpecializationSearchResults) {
        this.serviceAndSpecializationSearchResults = serviceAndSpecializationSearchResults;
    }


}