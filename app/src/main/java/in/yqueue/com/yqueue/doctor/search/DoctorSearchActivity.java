package in.yqueue.com.yqueue.doctor.search;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.doctor.search.model.ClinicSearchResultDTO;
import in.yqueue.com.yqueue.doctor.search.model.DoctorSearchResultDTO;
import in.yqueue.com.yqueue.doctor.search.model.SearchModel;
import in.yqueue.com.yqueue.doctor.search.model.ServiceAndSpecializationDTO;
import in.yqueue.com.yqueue.user.CurrentLocation;

public class DoctorSearchActivity extends CurrentLocation implements View.OnClickListener, SearchView.OnQueryTextListener {
    private Toolbar toolbar;
    private SearchView searchView;
    private ProgressDialog progressDialog;
    private AsyncTask<Void, Void, JSONObject> searchTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_search_activitiy);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Find Doctor");
        updateLocation();
        requestLocationFetch();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new MyProgressDialog().init(this, "", "", true);
        //Request Location Fetch from parent
        super.requestLocationFetch();

        //Activate on Click on custom Location
        RelativeLayout locationContainer = toolbar.findViewById(R.id.location_container);
        locationContainer.setOnClickListener(this);

        //Show default search suggestions
        showDefaultHints();

        searchView = findViewById(R.id.search);
        searchView.setOnQueryTextListener(this);
        searchView.setOnClickListener(this);

        //Apply on click on clear recent search
        findViewById(R.id.clear_search).setOnClickListener(this);

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                showDefaultHints();
                return false;
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.location_container:
                super.customLocation();
                break;
            case R.id.search:
                searchView.setIconified(false);
                setRecentSearch();
                break;
            case R.id.clear_search:
                clearRecentSearch();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("S", query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        findViewById(R.id.default_search_list_container).setVisibility(View.GONE);
        if (newText.length() >= 2) {
            Log.d("S", newText);
            if (searchTask != null && !searchTask.isCancelled()) {
                searchTask.cancel(true);
            }
            searchTask = new SearchDoctor(newText).execute();
            return false;
        } else {
            if (newText.length() == 0)
                setRecentSearch();
            return false;
        }
    }

    private void setList(SearchModel searchList) {
        findViewById(R.id.default_search_list_container).setVisibility(View.GONE);
        List<DoctorSearchResultDTO> doctorSearchResuls = searchList.getDoctorSearchResults();
        List<ClinicSearchResultDTO> clinicSearchResuls = searchList.getClinicSearchResults();
        List<ServiceAndSpecializationDTO> serviceSpecializationSearchResuls = searchList.getServiceAndSpecializationSearchResults();
        if (doctorSearchResuls.size() == 0 && clinicSearchResuls.size() == 0 && serviceSpecializationSearchResuls.size() == 0) {
            findViewById(R.id.doctors_search_list_container).setVisibility(View.GONE);
            findViewById(R.id.clinics_search_list_container).setVisibility(View.GONE);
            findViewById(R.id.service_specialization_search_list_container).setVisibility(View.GONE);
            findViewById(R.id.no_result_container).setVisibility(View.VISIBLE);
        } else {
            updateDoctorList(new ArrayList<>(doctorSearchResuls));
            updateServiceAndSpecializationList(new ArrayList<>(serviceSpecializationSearchResuls));
            updateClinicList(new ArrayList<>(clinicSearchResuls));
            findViewById(R.id.no_result_container).setVisibility(View.GONE);
        }
    }

    private void showDefaultHints() {
        findViewById(R.id.recent_list_heading).setVisibility(View.GONE);
        TextView defaultSearchHeading = findViewById(R.id.default_search_list_heading);
        defaultSearchHeading.setText("Find By Specialization");
        String[] doctorSearchSuggestion = Constants.DOCTOR_SEARCH_SUGGESTION;
        findViewById(R.id.clinics_search_list_container).setVisibility(View.GONE);
        findViewById(R.id.service_specialization_search_list_container).setVisibility(View.GONE);
        findViewById(R.id.doctors_search_list_container).setVisibility(View.GONE);
        findViewById(R.id.default_search_list_container).setVisibility(View.VISIBLE);
        RecyclerView searchSuggestionList = findViewById(R.id.default_search_list);
        SearchSuggestionsAdapter searchSuggestionsAdapter = new SearchSuggestionsAdapter(doctorSearchSuggestion);
        searchSuggestionList.setLayoutManager(new LinearLayoutManager(DoctorSearchActivity.this, LinearLayoutManager.VERTICAL, false));
        searchSuggestionList.setAdapter(searchSuggestionsAdapter);
    }

    private void setRecentSearch() {
        findViewById(R.id.clinics_search_list_heading).setVisibility(View.GONE);
        findViewById(R.id.doctors_search_list_heading).setVisibility(View.GONE);
        findViewById(R.id.service_specialization_search_list_heading).setVisibility(View.GONE);
        findViewById(R.id.recent_list_heading).setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.search_suggestion), Context.MODE_PRIVATE);
        String searchSuggestionString = sharedPreferences.getString(getString(R.string.search_suggestion), null);
        if (searchSuggestionString != null) {
            try {
                SearchModel searchModel = new ObjectMapper().readValue(searchSuggestionString, SearchModel.class);
                Collections.reverse(searchModel.getDoctorSearchResults());
                Collections.reverse(searchModel.getClinicSearchResults());
                Collections.reverse(searchModel.getServiceAndSpecializationSearchResults());
                setList(searchModel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            showDefaultHints();
        }
    }

    private void clearRecentSearch() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.search_suggestion), Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
        showDefaultHints();
    }

    private void updateDoctorList(List<DoctorSearchResultDTO> doctorSearchResultDTOS) {

        if (doctorSearchResultDTOS.size() > 0) {
            findViewById(R.id.doctors_search_list_container).setVisibility(View.VISIBLE);
            RecyclerView doctorSearchList = findViewById(R.id.doctors_search_list);
            DoctorSearchAdapter doctorSearchAdapter = new DoctorSearchAdapter(doctorSearchResultDTOS);
            doctorSearchList.setLayoutManager(new LinearLayoutManager(DoctorSearchActivity.this, LinearLayoutManager.VERTICAL, false));
            doctorSearchList.setAdapter(doctorSearchAdapter);
        } else {
            findViewById(R.id.doctors_search_list_container).setVisibility(View.GONE);
        }
    }

    private void updateServiceAndSpecializationList(List<ServiceAndSpecializationDTO> serviceSpecializationSearchResultDTOS) {
        if (serviceSpecializationSearchResultDTOS.size() > 0) {
            findViewById(R.id.service_specialization_search_list_container).setVisibility(View.VISIBLE);
            RecyclerView serviceSpecializationSearchList = findViewById(R.id.service_specialization_search_list);
            ServiceSpecializationSearchAdapter specializationSearchAdapter = new ServiceSpecializationSearchAdapter(serviceSpecializationSearchResultDTOS);
            serviceSpecializationSearchList.setLayoutManager(new LinearLayoutManager(DoctorSearchActivity.this, LinearLayoutManager.VERTICAL, false));
            serviceSpecializationSearchList.setAdapter(specializationSearchAdapter);
        } else {
            findViewById(R.id.service_specialization_search_list_container).setVisibility(View.GONE);
        }
    }

    private void updateClinicList(List<ClinicSearchResultDTO> clinicSearchResultDTOS) {
        if (clinicSearchResultDTOS.size() > 0) {
            findViewById(R.id.clinics_search_list_container).setVisibility(View.VISIBLE);
            RecyclerView clinicSearchList = findViewById(R.id.clinics_search_list);
            ClinicSearchAdapter clinicSearchAdapter = new ClinicSearchAdapter(clinicSearchResultDTOS);
            clinicSearchList.setLayoutManager(new LinearLayoutManager(DoctorSearchActivity.this, LinearLayoutManager.VERTICAL, false));
            clinicSearchList.setAdapter(clinicSearchAdapter);
        } else {
            findViewById(R.id.clinics_search_list_container).setVisibility(View.GONE);
        }
    }

    class SearchDoctor extends AsyncTask<Void, Void, JSONObject> {
        String url = Constants.API_SEARCH;

        SearchDoctor(String query) {
            url += "?keyword=" + query + "&city=jaipur";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!statusCode.trim().equalsIgnoreCase("200")) {
                    return;
                }
                try {
                    SearchModel searchList = (SearchModel) response.get("SearchList");
                    findViewById(R.id.recent_list_heading).setVisibility(View.GONE);
                    findViewById(R.id.clinics_search_list_heading).setVisibility(View.VISIBLE);
                    findViewById(R.id.doctors_search_list_heading).setVisibility(View.VISIBLE);
                    findViewById(R.id.service_specialization_search_list_heading).setVisibility(View.VISIBLE);
                    setList(searchList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                SearchModel responseEntity = RestService.instance().get(url, SearchModel.class);
                jsonObject.put("StatusCode", "200");
                jsonObject.put("SearchList", responseEntity);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return jsonObject;
        }
    }
}
