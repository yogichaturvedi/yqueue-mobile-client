package in.yqueue.com.yqueue.doctor;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.doctor.model.ClinicDetails;
import in.yqueue.com.yqueue.util.Utils;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class DoctorClinicAdapter extends RecyclerView.Adapter<DoctorClinicAdapter.ViewHolder> {
    private List<ClinicDetails> clinicDetails;

    public DoctorClinicAdapter(List<ClinicDetails> clinicDetails) {

        this.clinicDetails = clinicDetails;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DoctorClinicAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doctor_clinic_list_item, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView, clinicDetails);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        ClinicDetails clinic = clinicDetails.get(position);
        viewHolder.clinicName.setText(clinic.getName());
//        clinicLocality.setText(doctor.getClinics().get(0).getLocality());
        //availability.setText(doctor.getAB);
        String timeToDisplay = "Naveen has done some Mistakes";
        try {
            Date date1 = Utils.toCalendar(clinic.getStart());
            Date date2 = Utils.toCalendar(clinic.getEnd());
            String startTime = Utils.getParsedTime(date1);
            String endTime = Utils.getParsedTime(date2);
            timeToDisplay = startTime + " - " + endTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }


//        viewHolder.availableTime.setText(timeToDisplay);
        String address = Utils.parseAddress(clinic.getAddress());
        viewHolder.clinicAddress.setText(address);
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return clinicDetails.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView availableTime, clinicAddress, clinicName,mapDirection;
        private List<ClinicDetails> clinicDetails;

        public ViewHolder(View itemLayoutView, List<ClinicDetails> clinicDetails) {
            super(itemLayoutView);
//            availableTime = itemLayoutView.findViewById(R.id.available_time);
            clinicAddress = itemLayoutView.findViewById(R.id.clinic_address);
            clinicName = itemLayoutView.findViewById(R.id.clinic_name);
            mapDirection = itemLayoutView.findViewById(R.id.map_direction);
            mapDirection.setOnClickListener(this);
            this.clinicDetails = clinicDetails;
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.map_direction:
                    ClinicDetails clinicDetails = this.clinicDetails.get(getAdapterPosition());
                    double latitude = clinicDetails.getAddress().getLatitude();
                    double longitude = clinicDetails.getAddress().getLongitude();
                    String label = clinicDetails.getName();
                    if (latitude != 0.0 && longitude != 0.0) {
                        String uriBegin = "geo:" + latitude + "," + longitude;
                        String query = latitude + "," + longitude + "(" + label + ")";
                        String encodedQuery = Uri.encode(query);
                        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                        Uri uri = Uri.parse(uriString);
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                        view.getContext().startActivity(intent);
                    }
                    break;
            }
        }
    }
}
