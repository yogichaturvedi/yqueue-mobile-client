package in.yqueue.com.yqueue.user.appointments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.user.appointments.liveStatus.AppointmentLiveStatusActivity;

public class AppointmentDetailActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appointment_detail_activity);

        //Toolbar setup and title
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Appointment Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Set on click listener on map direction text view
        findViewById(R.id.map_direction).setOnClickListener(this);

        //Set on click listener on live status button
        findViewById(R.id.live_status).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.map_direction:
                double latitude = 26.8917823;
                double longitude = 75.7626572;
                String label = "Sai Clinic";
                String uriBegin = "geo:" + latitude + "," + longitude;
                String query = latitude + "," + longitude + "(" + label + ")";
                String encodedQuery = Uri.encode(query);
                String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                Uri uri = Uri.parse(uriString);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case R.id.live_status:
                Intent intent1 = new Intent(AppointmentDetailActivity.this, AppointmentLiveStatusActivity.class);
                startActivity(intent1);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
