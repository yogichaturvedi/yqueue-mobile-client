package in.yqueue.com.yqueue.doctor;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashMap;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.doctor.model.Address;
import in.yqueue.com.yqueue.doctor.model.DoctorBasicDetail;
import in.yqueue.com.yqueue.doctor.model.Element;
import in.yqueue.com.yqueue.doctor.model.GoogleMapResponse;
import in.yqueue.com.yqueue.doctor.model.Rows;
import in.yqueue.com.yqueue.doctor.model.TextValue;
import in.yqueue.com.yqueue.service.Move;

import static in.yqueue.com.yqueue.api.Constants.GOOGLE_MAP_API;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.ViewHolder> {
    private final double latitude;
    private final double longitude;
    private ArrayList<DoctorBasicDetail> doctorBasicDetails;

    public DoctorListAdapter(ArrayList<DoctorBasicDetail> doctorBasicDetails, double latitude, double longitude) {

        this.doctorBasicDetails = doctorBasicDetails;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DoctorListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doctor_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, doctorBasicDetails);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        DoctorBasicDetail doctorBasicDetail = doctorBasicDetails.get(position);
        viewHolder.name.setText("Dr. " + doctorBasicDetail.getName());
        viewHolder.availability.setText(doctorBasicDetail.getAvailability());
        String experienceToDisplay = "Exp. " + String.valueOf(doctorBasicDetail.getExperience() + " yrs");
        viewHolder.experience.setText(experienceToDisplay + "  " + " \u2022 ");
        String feeToDisplay = "₹ " + String.valueOf(doctorBasicDetail.getFees());
        viewHolder.fees.setText(feeToDisplay);
        viewHolder.clinicName.setText(doctorBasicDetail.getClinic());
        Address address = doctorBasicDetail.getAddress();
        viewHolder.location.setText(address.getLocality());
        viewHolder.specialization.setText(doctorBasicDetail.getSpecialization());
        if (latitude == 0 || longitude == 0) {
            viewHolder.distance.setVisibility(View.GONE);
        } else {
            viewHolder.distance.setVisibility(View.VISIBLE);
            viewHolder.distance.setText("~" + doctorBasicDetail.getAddress().getDistance());
            String distance = "~" + doctorBasicDetail.getAddress().getDistance() + " ";
            String statusToShow = distance + doctorBasicDetail.getAddress().getDuration() + " away";
            SpannableStringBuilder str = new SpannableStringBuilder(statusToShow);
            str.setSpan(new ForegroundColorSpan(Color.parseColor("#3F51B5")), distance.length(), statusToShow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            viewHolder.distance.setText(str);

        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return doctorBasicDetails.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ArrayList<DoctorBasicDetail> doctorBasicDetails;
        public TextView name;
        public TextView availability;
        public TextView specialization;
        public TextView distance;
        public TextView experience;
        public TextView fees;
        public TextView location;
        public TextView clinicName;

        public ViewHolder(View itemLayoutView, ArrayList<DoctorBasicDetail> doctorBasicDetails) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.doctor_name);
            availability = (TextView) itemLayoutView.findViewById(R.id.doctor_availability);
            experience = (TextView) itemLayoutView.findViewById(R.id.doctor_experience);
            fees = (TextView) itemLayoutView.findViewById(R.id.doctor_fee);
            clinicName = (TextView) itemLayoutView.findViewById(R.id.doctor_clinic);
            location = (TextView) itemLayoutView.findViewById(R.id.doctor_location);
            specialization = (TextView) itemLayoutView.findViewById(R.id.doctor_specialization);
            distance = (TextView) itemLayoutView.findViewById(R.id.doctor_distance);
            itemLayoutView.findViewById(R.id.button_book).setOnClickListener(this);
            this.doctorBasicDetails = doctorBasicDetails;
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            DoctorBasicDetail dotor = doctorBasicDetails.get(getAdapterPosition());
            String name = dotor.getName();
            Bundle bundle = new Bundle();
            String id = dotor.getId();
            bundle.putString("id", id);
            bundle.putString("name", name);
            new Move().activity(view.getContext(), DoctorDetailActivity.class, bundle, "doctor");
        }
    }

}
