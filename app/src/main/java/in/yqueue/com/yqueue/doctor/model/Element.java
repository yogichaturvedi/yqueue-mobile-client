package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Element{
    private TextValue distance;
    private TextValue duration;

    public void setDistance(TextValue distance) {
        this.distance = distance;
    }

    public void setDuration(TextValue duration) {
        this.duration = duration;
    }

    //getters

    public TextValue getDistance() {
        return distance;
    }

    public TextValue getDuration() {
        return duration;
    }
}
