package in.yqueue.com.yqueue.splashScreen;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import in.yqueue.com.yqueue.MainActivity;
import in.yqueue.com.yqueue.R;

public class SplashScreen extends AppCompatActivity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        ImageView imageView = (ImageView) findViewById(R.id.heart_image);
//        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
//        imageView.startAnimation(pulse);
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                imageView,
                PropertyValuesHolder.ofFloat("scaleX", .8f),
                PropertyValuesHolder.ofFloat("scaleY", .8f));
        scaleDown.setDuration(710);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.splash_screen_progress_bar).setVisibility(View.GONE);
            }
        }, 1);

        new Handler().postDelayed(new Runnable() {
 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                findViewById(R.id.splash_screen_progress_bar).setVisibility(View.GONE);
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


}
