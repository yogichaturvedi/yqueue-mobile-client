package in.yqueue.com.yqueue.user;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.yqueue.com.yqueue.R;

public class CurrentLocation extends AppCompatActivity implements LocationListener {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static boolean customLocationEnabled = false;
    private static List<Address> address;
    private static String customAddress = "";
    protected GeoDataClient mGeoDataClient;
    private LocationManager locationManager;
    private String provider;
    private PlaceAutocompleteFragment autocompleteFragment;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(this, null);
        setContentView(R.layout.user_current_location);
        beginSearch();
    }

    private void beginSearch() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            checkLocationPermission();
            return;
        }
        Location location = getLastKnownLocation();

        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(location);
        } else {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initToolbar();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onLocationChanged(Location location) {
//        Toast.makeText(this, "location.getLatitude() + \" : \" + location.getLongitude()", Toast.LENGTH_SHORT).show();
        if (customLocationEnabled) {
            return;
        }
        //You had this as int. It is advised to have Lat/Loing as double.
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        try {
            address = geoCoder.getFromLocation(lat, lng, 1);
            updateLocation();
        } catch (IOException e) {
            // Handle IOException
        } catch (NullPointerException e) {
            // Handle NullPointerException
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
//        Toast.makeText(this, status, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
//        Toast.makeText(this, "Enabled new provider " + provider,
//                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {


        if (customLocationEnabled && address != null && address.size() != 0) {
            return;
        }
//        Toast.makeText(this, "Disabled provider " + provider,
//                Toast.LENGTH_SHORT).show();
       showSettingsAlert();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // On pressing the cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    requestLocationFetch();
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        requestLocationFetch();
                    }

                } else {
                    TextView currentLocation = toolbar.findViewById(R.id.current_location);
                    customLocationEnabled = true;
                    customAddress = "Select";
                    currentLocation.setText("Select");
                    setSupportActionBar(toolbar);
                }
                return;
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                double latitude = place.getLatLng().latitude;
                double longitude = place.getLatLng().longitude;
                Geocoder geoCoder = new Geocoder(CurrentLocation.this, Locale.getDefault());
                StringBuilder builder = new StringBuilder();
                try {
                    address = geoCoder.getFromLocation(latitude, longitude, 1);

                    ArrayList<String> locationArray = new ArrayList<>();
                    Address addr = CurrentLocation.address.get(0);
                    if (addr.getSubLocality() != null)
                        locationArray.add(addr.getSubLocality());
                    if (addr.getLocality() != null)
                        locationArray.add(addr.getLocality());
                    if (addr.getSubAdminArea() != null)
                        locationArray.add(addr.getSubAdminArea());
                    if (addr.getAdminArea() != null)
                        locationArray.add(addr.getAdminArea());

                    if (locationArray.size() == 1) {
                        address.get(0).setSubLocality(locationArray.get(0));
                        address.get(0).setLocality(locationArray.get(1));
                    } else if (locationArray.size() >= 2) {
                        address.get(0).setSubLocality(locationArray.get(0));
                        address.get(0).setLocality(locationArray.get(1));
                    } else {
                        address.get(0).setSubLocality("");
                        address.get(0).setLocality("");
                    }

                    customLocationEnabled = true;
                    customAddress = place.getName().toString();
                    updateLocation();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    public boolean checkLocationPermission() {
        if (customLocationEnabled) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(CurrentLocation.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    public void updateLocation() {
        if (customLocationEnabled) {
            TextView currentLocation = toolbar.findViewById(R.id.current_location);
            currentLocation.setText((customAddress != null && !customAddress.trim().equals("")) ? customAddress : "Select");
            setSupportActionBar(toolbar);
            return;
        } else if (address == null) {
            Location location = getLastKnownLocation();
            Geocoder geoCoder = new Geocoder(CurrentLocation.this, Locale.getDefault());
            StringBuilder builder = new StringBuilder();

            try {
                if (location != null) {
                    address = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            requestLocationFetch();
        } else if (address.size() != 0 && toolbar != null) {
            TextView currentLocation = toolbar.findViewById(R.id.current_location);
            Address addrs = address.get(0);
            String addr = !addrs.getSubLocality().equals("") ? (addrs.getSubLocality() + ", ") : "";
            addr = addr + addrs.getLocality();
            currentLocation.setText(addr);
            setSupportActionBar(toolbar);
        }
    }

    public void requestLocationFetch() {
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
//        Toast.makeText(this, "Before Begin", Toast.LENGTH_SHORT).show();

        beginSearch();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    public void customLocation() {

        int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
        try {
            AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder().setCountry("IN").setTypeFilter(4).build();
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(autocompleteFilter)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }


}