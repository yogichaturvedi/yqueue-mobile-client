package in.yqueue.com.yqueue.book;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.util.Utils;

public class BookDateActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {

            setCustomDate("Select");

            FragmentLifecycle fragmentToShow = (FragmentLifecycle) adapter.getItem(newPosition);
            fragmentToShow.onResumeFragment();

            FragmentLifecycle fragmentToHide = (FragmentLifecycle) adapter.getItem(currentPosition);
            fragmentToHide.onPauseFragment();

            currentPosition = newPosition;
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_date_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Choose a time slot");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d("", tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.d("", tab.getText().toString());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2) {
                    Log.d("", tab.getText().toString());
                    FragmentLifecycle fragmentToShow = (FragmentLifecycle) adapter.getItem(2);
                    fragmentToShow.onResumeFragment();
                }
            }
        });

        setCustomTab();
    }

    private void setCustomTab() {
        RelativeLayout tabOne = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.book_custom_cal_date_tab, null);
        TextView date1 = tabOne.findViewById(R.id.date);
        TextView day1 = tabOne.findViewById(R.id.label);
        int date = new Date().getDate();
        date1.setText(String.valueOf(date));
        day1.setText("Today");
        tabLayout.getTabAt(0).setCustomView(tabOne);

        RelativeLayout tabTwo = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.book_custom_cal_date_tab, null);
        TextView date2 = tabTwo.findViewById(R.id.date);
        TextView day2 = tabTwo.findViewById(R.id.label);
        Date nextDate = Utils.getNextDate(new Date());
        date2.setText(String.valueOf(nextDate.getDate()));
        day2.setText("Tomorrow");
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        RelativeLayout tabThree = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.book_custom_cal_date_tab, null);
        tabThree.findViewById(R.id.default_date).setVisibility(View.GONE);
        tabThree.findViewById(R.id.custom_date).setVisibility(View.VISIBLE);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        BookTodayFragment todayFragment = new BookTodayFragment();
        BookTomorrowFragment tomorrowFragment = new BookTomorrowFragment();
        BookCustomDateFragment customDateFragment = new BookCustomDateFragment();

        //Send doctorId, clinicId to fragments
        Bundle bundle = new Bundle();
        Bundle bundleExtra = getIntent().getBundleExtra("book-info");
        if (bundleExtra != null) {
            String doctorId = bundleExtra.getString("doctorId");
            String clinicId = bundleExtra.getString("clinicId");
            bundle.putString("doctorId", doctorId);
            bundle.putString("clinicId", clinicId);
        }
        todayFragment.setArguments(bundle);
        tomorrowFragment.setArguments(bundle);
        customDateFragment.setArguments(bundle);

        //Add Fragments to view pager adapter
        adapter.addFragment(todayFragment, "TODAY");
        adapter.addFragment(tomorrowFragment, "TOMORROW");
        adapter.addFragment(customDateFragment, "CUSTOM");
        viewPager.addOnPageChangeListener(pageChangeListener);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setCustomDate(String date) {

        RelativeLayout tabThree = (RelativeLayout) LayoutInflater.from(BookDateActivity.this).inflate(R.layout.book_custom_cal_date_tab, null);
        tabThree.findViewById(R.id.default_date).setVisibility(View.GONE);
        RelativeLayout view = tabThree.findViewById(R.id.custom_date);
        view.setVisibility(View.VISIBLE);
        TextView calDate = tabThree.findViewById(R.id.calendar);
        calDate.setText(date);
        tabLayout.getTabAt(2).setCustomView(null);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
