//package in.yqueue.com.yqueue.api;
//
//import android.app.MyProgressDialog;
//import android.os.AsyncTask;
//
//import com.okruti.ecart.model.MagentoProduct;
//
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.http.converter.StringHttpMessageConverter;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.Arrays;
//
///**
// * Created by Yogesh on 28-09-2016.
// */
//public class MagentoService {
//
//    private RestTemplate restTemplate;
//
//    public void getAllProducts() {
//        String url = "http://192.168.111.109:90/ecart/rest/V1/categories";
//        HttpExchange<MagentoProduct> obj = new HttpExchange<MagentoProduct>(url, MagentoProduct.class);
//        obj.execute();
//
//
//    }
//
//    public class HttpExchange<T> extends AsyncTask<Void, Void, T> {
//        private String url;
//        private Class<T> responseType;
//        public MyProgressDialog progressDialog;
//
//        public HttpExchange(String url, Class<T> responseType) {
//            System.out.println("1111111111111111111111111111111111 HttpExchange");
//            this.url = url;
//            this.responseType = responseType;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            System.out.println("2222222222222222222222222222222222 HttpExchange");
//
//            super.onPreExecute();
//        }
//
//        @Override
//        protected void onPostExecute(T t) {
//            super.onPostExecute(t);
//        }
//
//        @Override
//        protected T doInBackground(Void... params) {
//            restTemplate = new RestTemplate();
//            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
//
//            HttpHeaders headers = new HttpHeaders();
//            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//            headers.set("Authorization", "OAuth oauth_consumer_key=\"fxucjg4eonuci2cae05i7kyht9xobyga\",oauth_token=\"u08f0i657sc5b1am0isny750c6htj0kk\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1475152431\",oauth_nonce=\"JALTws\",oauth_version=\"1.0\",oauth_signature=\"IZqGRrwxeI51aetJInXWqAxZ0QM%3D\"");
//            HttpEntity entity = new HttpEntity(headers);
//            ResponseEntity exchange = restTemplate.exchange(url, HttpMethod.GET, entity, MagentoProduct.class);
//            exchange.getBody();
//            return null;
//        }
//    }
//
//}
