package in.yqueue.com.yqueue.user;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import in.yqueue.com.yqueue.MainActivity;
import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.TypefaceUtil;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.common.MySnackBar;
import in.yqueue.com.yqueue.service.Move;
import in.yqueue.com.yqueue.util.Utils;

public class UserProfileEdit extends AppCompatActivity implements View.OnClickListener {
    static final int PICTURE = 1;
    static final int REQUEST_IMAGE_CAPTURE = 125;
    static final int CAMERA_REQUEST_CODE = 123;
    static final int GALLERY_CODE = 124;
    static final int STORAGE_PERMISSION = 145;
    private String mCurrentPhotoPath = "";
    private ImageView userImageview;
    private EditText nameInput, mobileNumberInput, emailInput, ageInput;
    private RadioButton maleRadioButton, femaleRadioButton;
    private User user;
    private Button signUpButton;
    private String imageFile;
    private String TAG = "UserProfileEdit Activity";
    private BottomSheetDialog mBottomSheetDialog;
    private String removeImage;
    private boolean IMAGE_MODIFY = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_edit);


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
//        findViewById(R.id.user_image_upload).setOnClickListener(this);
        userImageview = findViewById(R.id.user_image);
        userImageview.setOnClickListener(this);
        getIds();

        //Fetching User from shared preferences
        try {
            user = new ObjectMapper().readValue(getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE).getString(String.valueOf(R.string.userSP_user_key), ""), User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Find Id of Signup button and setting on click listener on it
        signUpButton = findViewById(R.id.btn_signup);
        signUpButton.setOnClickListener(this);
        if (user != null) {
            fillForm(this.user);
            signUpButton.setText("Update");
        } else {
            signUpButton.setText("Sign Up");
            Object o = getIntent().getBundleExtra("user").get("user");
            if (o instanceof User) {
                user = (User) o;
                String contactNumber = "+" + user.getCountryCode() + user.getContactNumber();
                mobileNumberInput.setText(contactNumber);
            } else {
                //TODO:Error handling
            }
        }
        mobileNumberInput.setEnabled(false);
    }

    private void getIds() {
        nameInput = findViewById(R.id.input_name);
        ageInput = findViewById(R.id.input_age);
        mobileNumberInput = findViewById(R.id.input_mobile_number);
        emailInput = findViewById(R.id.input_email);
        maleRadioButton = findViewById(R.id.male_radio_button);
        femaleRadioButton = findViewById(R.id.female_radio_button);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_image:
                showImageUploadDialog();
                break;
            case R.id.btn_signup:
                onSubmitForm();
                break;
            case R.id.camera_view:
                mBottomSheetDialog.dismiss();
                checkCameraAndStoragePermission();
                IMAGE_MODIFY = true;
                break;
            case R.id.gallery_view:
                mBottomSheetDialog.dismiss();
                openGallery();
                break;
            case R.id.trash_view:
                mBottomSheetDialog.dismiss();
                userImageview.setImageURI(null);
                userImageview.setImageResource(R.drawable.user_no_image);
                removeImage = user.getImage();
                user.setImage("");
                break;
            case R.id.cancel:
                mBottomSheetDialog.dismiss();
                break;

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCurrentPhotoPath != null) {
            File fdelete = new File(mCurrentPhotoPath);
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                    System.out.println("file Deleted :" + mCurrentPhotoPath);
                } else {
                    System.out.println("file not Deleted :" + mCurrentPhotoPath);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void onSubmitForm() {
        if (!(checkEmpty("Name", nameInput) ||
                checkEmpty("Email", emailInput) ||
                validateEmail(emailInput) ||
                checkEmpty("Age", ageInput) ||
                validateAge(ageInput))) {
            user.setName(nameInput.getText().toString());
            user.setEmail(emailInput.getText().toString());
            user.setAge(Integer.parseInt((ageInput.getText().toString())));
            user.setGender("Female");
            if (maleRadioButton.isChecked()) {
                user.setGender("Male");
            }
            saveImage();
        }
    }

    private boolean validateAge(EditText ageInput) {
        int age = Integer.parseInt(ageInput.getText().toString());
        if (age <= 0 || age >= 130) {
            ageInput.setError("Not valid age");
            return true;
        }
        return false;
    }

    private boolean validateEmail(EditText emailInput) {
        String target = emailInput.getText().toString();
        if (!Patterns.EMAIL_ADDRESS.matcher(target).matches()) {
            emailInput.setError("Enter valid email");
            return true;
        }
        return false;
    }

    private boolean checkEmpty(String title, EditText field) {
        if (field.getText().toString().trim().equals("")) {
            field.setError(title + " cannot be blank");
            return true;
        }
        return false;
    }

    private void fillForm(User user) {
        nameInput.setText(user.getName());
        String contactNumber = "+" + user.getCountryCode() + user.getContactNumber();
        mobileNumberInput.setText(contactNumber);
        emailInput.setText(user.getEmail());
        ageInput.setText(String.valueOf(user.getAge()));
        if (user.getImage() != null && !user.getImage().equals("")) {
            userImageview.setImageURI(null);
            Picasso.Builder picassoBuilder = new Picasso.Builder(this);
            Picasso picasso = picassoBuilder.build();
            picasso.setIndicatorsEnabled(true);
            picasso.load(Constants.API_IMAGE_PATIENT + user.getImage()).networkPolicy(NetworkPolicy.NO_CACHE).placeholder(R.drawable.progress_animation).error(R.drawable.user_no_image).into(userImageview);
        } else {
            userImageview.setImageURI(null);
            userImageview.setImageResource(R.drawable.user_no_image);
        }
        if (user.getGender() != null && user.getGender().trim().equalsIgnoreCase("female")) {
            femaleRadioButton.setChecked(true);
            maleRadioButton.setChecked(false);
        } else {
            femaleRadioButton.setChecked(false);
            maleRadioButton.setChecked(true);
        }
    }

    private void showImageUploadDialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.image_picker_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();
        //Set on click on gallery, image and trash
        sheetView.findViewById(R.id.camera_view).setOnClickListener(this);
        sheetView.findViewById(R.id.gallery_view).setOnClickListener(this);
        sheetView.findViewById(R.id.trash_view).setOnClickListener(this);
        sheetView.findViewById(R.id.cancel).setOnClickListener(this);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), GALLERY_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == GALLERY_CODE || requestCode == Crop.REQUEST_PICK) && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            File f = new File(mCurrentPhotoPath);
            Uri contentUri = Uri.fromFile(f);
            beginCrop(contentUri);
        }
    }

    public boolean checkCameraAndStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                   /* && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED*/) {
                dispatchTakePictureIntent();
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            dispatchTakePictureIntent();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "in.yqueue.com.yqueue",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(200, 200).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            userImageview.setImageURI(null);
            userImageview.setImageURI(Crop.getOutput(result));
            IMAGE_MODIFY = true;
            imageFile = Crop.getOutput(result).getPath();
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(UserProfileEdit.this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void savePatient() {
        ProgressDialog saveUserProgressDialog = new MyProgressDialog().init(UserProfileEdit.this, "", "", false);
        new SavePatient(saveUserProgressDialog).execute();
    }

    private void saveImage() {
        if (!IMAGE_MODIFY) {
            savePatient();
            return;
        }
        ProgressDialog saveImageProgressDialog = new MyProgressDialog().init(UserProfileEdit.this, "", "", false);
        new SaveImage(saveImageProgressDialog).execute();
    }

    class SavePatient extends AsyncTask<Void, Void, JSONObject> {

        private ProgressDialog progressDialog;
        private String url = Constants.API_PATIENT;

        SavePatient(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Callable callable = new Callable() {
                    @Override
                    public Object call() throws Exception {
                        savePatient();
                        return null;
                    }
                };
                if (!statusCode.trim().equalsIgnoreCase("200")) {
                    new MySnackBar(findViewById(R.id.user_image), Utils.getErrorMessage(statusCode), "indeterminant", Color.YELLOW, "RETRY", callable);
                    return;
                }
            }
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            try {
                try {
                    editor.putString(String.valueOf(R.string.userSP_user_key), new ObjectMapper().writeValueAsString((User) response.get("response")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                editor.apply();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            if (signUpButton.getText().toString().equalsIgnoreCase("update")) {
                Bundle bundle = new Bundle();
                bundle.putString("fragmentName", "profile");
                new Move().activity(UserProfileEdit.this, MainActivity.class, bundle, "fragmentName");
                finish();
            } else {
                new Move().activity(UserProfileEdit.this, MainActivity.class);
                finish();
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                User response = RestService.instance().post(url, user, User.class);
                jsonObject.put("StatusCode", "200");
                jsonObject.put("response", response);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Error in saving patient", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }

    class SaveImage extends AsyncTask<Void, Void, JSONObject> {

        private ProgressDialog progressDialog;
        private String url = Constants.API_IMAGE_PATIENT;

        SaveImage(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Callable callable = new Callable() {
                    @Override
                    public Object call() throws Exception {
                        saveImage();
                        return null;
                    }
                };
                if (statusCode.trim().equalsIgnoreCase("201")) {
                    try {
                        user.setImage(String.valueOf(response.get("location")));
                        savePatient();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (statusCode.trim().equalsIgnoreCase("200")) {
                    user.setImage("");
                    savePatient();
                } else {
                    new MySnackBar(findViewById(R.id.user_image), Utils.getErrorMessage(statusCode), "indeterminant", Color.YELLOW, "RETRY", callable);
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
                if (signUpButton.getText().toString().equalsIgnoreCase("update")) {
                    if (removeImage != null && !removeImage.equalsIgnoreCase("")) {
                        map.add("remove", removeImage);
                    } else {
                        map.add("update", user.getImage());
                        map.add("image", new FileSystemResource(imageFile));
                    }

                } else {
                    map.add("image", new FileSystemResource(imageFile));
                }
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.MULTIPART_FORM_DATA);
                HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(
                        map, headers);
                ResponseEntity<String> result = new RestTemplate().exchange(
                        url, HttpMethod.POST, requestEntity,
                        String.class);
                if (removeImage != null && !removeImage.equalsIgnoreCase("") && result.getStatusCode() == HttpStatus.OK) {
                    jsonObject.put("StatusCode", "200");
                    jsonObject.put("response", "Image Deleted Successfully");
                } else {
                    jsonObject.put("StatusCode", "201");
                    jsonObject.put("location", result.getHeaders().get("location").get(0));
                }
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Patient Image Upload", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }
}
