package in.yqueue.com.yqueue.book.timeSlot;

import java.util.ArrayList;

/**
 * Created by Naveen Kumawat on 16-04-2018.
 */

public class GroupInfo {

    private String timeSlot;
    private ArrayList<ChildInfo> childInfos = new ArrayList<ChildInfo>();

    public GroupInfo(String timeSlot, ArrayList<ChildInfo> childInfos) {
        this.timeSlot = timeSlot;
        this.childInfos = childInfos;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public ArrayList<ChildInfo> getChildInfos() {
        return childInfos;
    }

    public void setChildInfos(ArrayList<ChildInfo> childInfos) {
        this.childInfos = childInfos;
    }
}
