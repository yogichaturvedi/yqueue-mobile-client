package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TextValue{
    private String text;
    private  String value;

    public void setText(String text) {
        this.text = text;
    }

    public void setValue(String value) {
        this.value = value;
    }

    //getters

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }
}
