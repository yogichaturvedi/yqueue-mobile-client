package in.yqueue.com.yqueue.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.yqueue.com.yqueue.R;

public class NotificationListFragment extends Fragment implements View.OnClickListener {

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.notification_list_fragment, container, false);
//        DoctorListAdapter doctorListAdapter = new DoctorListAdapter(getList());
//        RecyclerView recyclerView = view.findViewById(R.id.doctors_list);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        recyclerView.setAdapter(doctorListAdapter);


        return view;
    }

    @Override
    public void onClick(View view) {
    }

//    public ArrayList<Doctor> getList() {
//        ArrayList<Doctor> doctorBasicDetails = new ArrayList<>();
//        int i = 0;
//        Doctor doctor = new Doctor("Amit Sirohiya", "amitsirohiya22@gmail.com", "", "Gynecologist", 400, 49.2, "Tilawaada Janana Hospital", "Tilawaada, Jaipur", "Available till 8 PM");
//        while (i <= 10) {
//            i += 1;
//            doctor.setName(doctor.getName());
//            doctorBasicDetails.add(doctor);
//        }
//        return doctorBasicDetails;
//    }
}