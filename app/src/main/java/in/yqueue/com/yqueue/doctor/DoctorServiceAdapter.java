package in.yqueue.com.yqueue.doctor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.doctor.model.Service;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class DoctorServiceAdapter extends RecyclerView.Adapter<DoctorServiceAdapter.ViewHolder> {
    private List<Service> services;
    private int layout;

    public DoctorServiceAdapter(List<Service> services, int layout) {
        this.services = services;
        this.layout = layout;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DoctorServiceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(layout, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        viewHolder.name.setText(services.get(position).getName());
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return services.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.service);
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
        }
    }
}
