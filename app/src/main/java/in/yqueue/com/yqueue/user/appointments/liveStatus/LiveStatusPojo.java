package in.yqueue.com.yqueue.user.appointments.liveStatus;

/**
 * Created by Naveen Kumawat on 19-04-2018.
 */

public class LiveStatusPojo {
    private int tokenNumber;
    private String status;
    private boolean isActive;
    private String time;

    public LiveStatusPojo(int tokenNumber, String status, boolean isActive, String time) {
        this.tokenNumber = tokenNumber;
        this.status = status;
        this.isActive = isActive;
        this.time = time;
    }

    public int getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(int tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
