package in.yqueue.com.yqueue.util;

import android.location.Location;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import in.yqueue.com.yqueue.doctor.model.Address;

/**
 * Created by Yogesh Chaturvedi on 22-04-2018.
 */

public class Utils {

    public static String getErrorMessage(String errorCode) {
        errorCode = errorCode.trim();
        if (errorCode.equalsIgnoreCase("404")) {
            return "Something went wrong";
        }
        return "Unable to proceed your request";
    }

    /**
     * Transform ISO 8601 string to Calendar.
     */
    public static Date toCalendar(final String iso8601string)
            throws ParseException {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH);
        dateFormat.setTimeZone(tz);
        return dateFormat.parse(iso8601string);
    }

    public static String getParsedTime(Date date) {
        int hours = date.getHours();
        String amPm = "AM";
        if (hours == 12) {
            amPm = "PM";
        } else if (hours > 12) {
            hours = hours - 12;
            amPm = "PM";
        }
        return (hours < 10 ? ("0" + hours) : hours) + ":" + (date.getMinutes() < 10 ? ("0" + date.getMinutes()) : date.getMinutes()) + " " + amPm;
    }

    public static String toISOString(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH);
        dateFormat.setTimeZone(tz);
        return dateFormat.format(date);
    }

    public static Date getNextDate(Date date) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }

    public static String parseAddress(Address address) {
        ArrayList<String> array = new ArrayList<>();
        if (!address.getStreet().equals("")) {
            array.add(address.getStreet());
        }
        if (!address.getLocality().equals("")) {
            array.add(address.getLocality());
        }
        if (!address.getCity().equals("")) {
            array.add(address.getCity());
        }

        if (!address.getState().equals("")) {
            array.add(address.getState());
        }

        if (!address.getPinCode().equals("")) {
            array.add(address.getPinCode());
        }

        StringBuilder response = new StringBuilder();
        for (int i = 0; i < array.size() - 1; i++) {
            response.append(array.get(i)).append(", ");
        }
        return response.toString();
    }

    public static float distance(double lat1, double lon1, double lat2, double lon2) {
//        double theta = lon1 - lon2;
//        double dist = Math.sin(deg2rad(lat1))
//                * Math.sin(deg2rad(lat2))
//                + Math.cos(deg2rad(lat1))
//                * Math.cos(deg2rad(lat2))
//                * Math.cos(deg2rad(theta));
//        dist = Math.acos(dist);
//        dist = rad2deg(dist);
//        dist = dist * 60 * 1.1515 * 1.60934;
//        return (dist);
        float[] results = new float[5];
        //26.813341, 75.760869 Sanganer
        //26.880784, 75.753185 My Location
        Location.distanceBetween(lat1, lon1, lat2, lon2, results);
        return results[0];
    }

    public static String parseDistance(float v) {
        if (v > 1000) {
            v = v / 1000;
            DecimalFormat df = new DecimalFormat("#.#");
            df.setRoundingMode(RoundingMode.CEILING);
            String dist = df.format(v);
            return dist+ " KM";
        } else {
            DecimalFormat df = new DecimalFormat("#.###");
            df.setRoundingMode(RoundingMode.CEILING);
            String dist = df.format(v);
            return dist + " Meters";
        }
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
