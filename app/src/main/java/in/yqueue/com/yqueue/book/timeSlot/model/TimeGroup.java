package in.yqueue.com.yqueue.book.timeSlot.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Yogesh Chaturvedi on 28-04-2018.
 */

public class TimeGroup {
    private String id;
    private String start;
    private String end;
    private String label;
    private boolean isAvailable;
    private List<TimeSlots> timeSlots;

    public TimeGroup(@JsonProperty("id") String id,
                     @JsonProperty("start") String start,
                     @JsonProperty("end") String end,
                     @JsonProperty("label") String label,
                     @JsonProperty("isAvailable") boolean isAvailable,
                     @JsonProperty("timeSlots") List<TimeSlots> timeSlots) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.label = label;
        this.isAvailable = isAvailable;
        this.timeSlots = timeSlots;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public List<TimeSlots> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(List<TimeSlots> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}