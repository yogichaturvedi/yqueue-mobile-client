package in.yqueue.com.yqueue.common;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.util.concurrent.Callable;

/**
 * Created by Naveen Kumawat on 18-04-2018.
 */

public class DialogBoxFrag {

    private final AlertDialog.Builder builder;

    public DialogBoxFrag(Context context, String title, String message, int icon, boolean cancelable, String positiveButtonText, String negativeButtonText, final Callable<Void> positiveFunction, final Callable<Void> negativeFunction) {

        builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            positiveFunction.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            negativeFunction.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        if (icon != 0) {
            builder.setIcon(icon);
        }
        builder.setCancelable(cancelable);
        builder.show();
    }
}
