package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Rows{
    private List<Element> elements;

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    //getters

     public List<Element> getElements() {
         return elements;
     }
 }
