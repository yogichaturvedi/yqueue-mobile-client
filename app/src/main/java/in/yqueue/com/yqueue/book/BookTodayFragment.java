package in.yqueue.com.yqueue.book;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.Callable;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.book.timeSlot.TimeSlotAdapter;
import in.yqueue.com.yqueue.book.timeSlot.model.TimeGroup;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.common.MySnackBar;
import in.yqueue.com.yqueue.user.User;
import in.yqueue.com.yqueue.util.Utils;

/**
 * Created by Naveen Kumawat on 16-04-2018.
 */

public class BookTodayFragment extends Fragment implements FragmentLifecycle {
    private String doctorId, clinicId, dateISOString;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.book_today_fragment, container, false);
        fetchTimeSlots();
        return view;
    }

    @Override
    public void onPauseFragment() {
        Log.i("", "onPauseFragment()");
    }

    @Override
    public void onResumeFragment() {
        Log.i("", "onResumeFragment()");
    }


    private void fetchTimeSlots() {
        if (getArguments() != null) {
            doctorId = getArguments().getString("doctorId");
            clinicId = getArguments().getString("clinicId");
        }
        Date date = new Date();
        dateISOString = Utils.toISOString(date);
        if (doctorId != null && clinicId != null && dateISOString != null) {
            ProgressDialog fetchTimeSlotProgressDialog = new MyProgressDialog().init(getActivity(), "", "", true);
            new FetchTimeSlots(fetchTimeSlotProgressDialog).execute();
        }
    }


    private void updateListView(ArrayList<TimeGroup> timeGroups) {
        ExpandableListView expandableListView = view.findViewById(R.id.book_today_expandable_list);
        String user = getActivity().getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE).getString(String.valueOf(R.string.userSP_user_key), null);
        if (timeGroups.size() == 0) {
            expandableListView.setVisibility(View.GONE);
            view.findViewById(R.id.not_available_container).setVisibility(View.VISIBLE);
            return;
        } else {
            expandableListView.setVisibility(View.VISIBLE);
            view.findViewById(R.id.not_available_container).setVisibility(View.GONE);
        }
        if (user != null) {
            try {
                User user1 = new ObjectMapper().readValue(user, User.class);
                TimeSlotAdapter timeSlotAdapter = new TimeSlotAdapter(getContext(), timeGroups, user1.getId(), doctorId, clinicId);
                expandableListView.setAdapter(timeSlotAdapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class FetchTimeSlots extends AsyncTask<Void, Void, JSONObject> {
                String url = Constants.API_DOCTOR + doctorId + "/clinic/" + clinicId + "/timeSlots?date=" + dateISOString;
//        String url = Constants.CONSTANT;
        ProgressDialog progressDialog;

        public FetchTimeSlots(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!statusCode.trim().equalsIgnoreCase("200")) {
                    Callable callable = new Callable() {
                        @Override
                        public Object call() throws Exception {
                            fetchTimeSlots();
                            return null;
                        }
                    };
                    new MySnackBar(view.findViewById(R.id.book_today_expandable_list), Utils.getErrorMessage(statusCode), "indeterminant", Color.YELLOW, "RETRY", callable);
                    return;
                }
                try {
                    TimeGroup[] responses = (TimeGroup[]) response.get("response");
                    ArrayList<TimeGroup> timeGroups = new ArrayList<>(Arrays.asList(responses));
                    updateListView(timeGroups);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                TimeGroup[] timeGroups = RestService.instance().get(url, TimeGroup[].class);
                jsonObject.put("StatusCode", "200");
                jsonObject.put("response", timeGroups);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Error fetching doctor", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }
}
