package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Set;

/**
 * Created by Yogesh Chaturvedi on 23-04-2018.
 */

public  class ClinicDetails {
    private String id;
    private Set<ContactInfo> contactInfo;
    private String name;
    private List<Service> services;
    private String start;
    private String end;
    private Address address;
    private boolean primaryFlag;

    public ClinicDetails(@JsonProperty("id") String id,
                         @JsonProperty("contactInfo") Set<ContactInfo> contactInfo,
                         @JsonProperty("name") String name,
                         @JsonProperty("services") List<Service> services,
                         @JsonProperty("start") String start,
                         @JsonProperty("end") String end,
                         @JsonProperty("address") Address address,
                         @JsonProperty("primaryFlag") boolean primaryFlag) {
        this.id = id;
        this.contactInfo = contactInfo;
        this.name = name;
        this.services = services;
        this.start = start;
        this.end = end;
        this.address = address;
        this.primaryFlag = primaryFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<ContactInfo> getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(Set<ContactInfo> contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isPrimaryFlag() {
        return primaryFlag;
    }

    public void setPrimaryFlag(boolean primaryFlag) {
        this.primaryFlag = primaryFlag;
    }
}