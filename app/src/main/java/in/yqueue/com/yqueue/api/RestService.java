package in.yqueue.com.yqueue.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * Created by Yogesh on 08-07-2016.
 */
public class RestService {

    private static RestService _instance;
    private final RestTemplate restTemplate;

    private RestService() {
        restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
        restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
    }

    public static RestService instance() {
        if (_instance == null) {
            _instance = new RestService();
        }
        return _instance;
    }

    public <T> T get(String url, Class<T> responseType, Object... urlVariables) {
        return restTemplate.getForObject(url, responseType, urlVariables);
    }

    public <T> T post(String url, Object request, Class<T> responseType, Object... uriVariables) {
        return restTemplate.postForObject(url, request, responseType, uriVariables);
    }

    public HttpHeaders getHeaders(String url, Object... uriVariables) {
        return restTemplate.headForHeaders(url, uriVariables);
    }

    public void put(String url, Object request) {
        restTemplate.put(url, request);
    }

}
