package in.yqueue.com.yqueue.doctor;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.book.BookDateActivity;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.common.MySnackBar;
import in.yqueue.com.yqueue.doctor.model.ClinicDetails;
import in.yqueue.com.yqueue.doctor.model.Doctor;
import in.yqueue.com.yqueue.doctor.model.Service;
import in.yqueue.com.yqueue.service.Move;
import in.yqueue.com.yqueue.util.Utils;

public class DoctorDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private Doctor doctor;
    private TextView name, specialization, clinicLocality, availability, availableTime, clinicAddress, clinicName;
    private AsyncTask<Void, Void, JSONObject> doctorFetchingAsyncTsk;
    private String doctorId, doctorName;
    private ClinicDetails primaryClinic;
    private RecyclerView clinicList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_detail_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Doctor detail");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView mapDirection = findViewById(R.id.map_direction);
        findViewById(R.id.appointment_button).setOnClickListener(this);
        findViewById(R.id.more_services).setOnClickListener(this);
        mapDirection.setOnClickListener(this);

        findIds();
        fetchDoctor();
    }

    public void fetchDoctor() {
        if (getIntent().getBundleExtra("doctor") != null) {
            doctorName = getIntent().getBundleExtra("doctor").getString("name");
            getSupportActionBar().setTitle(doctorName);
        }
        doctorId = "";
        if (getIntent().getBundleExtra("doctor") != null) {
            doctorId = getIntent().getBundleExtra("doctor").getString("id");
            if (doctorId != null && !doctorId.trim().equals("")) {
                Callable callable = new Callable() {
                    @Override
                    public Object call() throws Exception {
//                        doctorFetchingAsyncTsk.cancel(true);
                        return null;
                    }
                };
                ProgressDialog progressDialog = new MyProgressDialog().init(DoctorDetailActivity.this, "", "", true, callable);
                doctorFetchingAsyncTsk = new FetchDoctor(progressDialog).execute();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.map_direction:
                double latitude = primaryClinic.getAddress().getLatitude();
                double longitude = primaryClinic.getAddress().getLongitude();
                String label = primaryClinic.getName();
                if (latitude != 0.0 && longitude != 0.0) {
                    String uriBegin = "geo:" + latitude + "," + longitude;
                    String query = latitude + "," + longitude + "(" + label + ")";
                    String encodedQuery = Uri.encode(query);
                    String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                    Uri uri = Uri.parse(uriString);
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                break;
            case R.id.appointment_button:
                Bundle bundle = new Bundle();
                bundle.putString("doctorId", doctorId);
                bundle.putString("clinicId", primaryClinic.getId());
                new Move().activity(this, BookDateActivity.class, bundle, "book-info");
                break;

            case R.id.more_services:
                Bundle bundle1 = new Bundle();
                try {
                    String servicesAsString = new ObjectMapper().writeValueAsString(primaryClinic.getServices());
                    bundle1.putString("services", servicesAsString);
                    bundle1.putString("doctorName", doctor.getName());
                    new Move().activity(DoctorDetailActivity.this, DoctorServiceActivity.class, bundle1, "services");
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void findIds() {
        name = findViewById(R.id.doctor_name);
        specialization = findViewById(R.id.doctor_specialization);
//        clinicLocality = findViewById(R.id.clinic_locality);
        availability = findViewById(R.id.doctor_availability);
        availableTime = findViewById(R.id.available_time);
        clinicAddress = findViewById(R.id.clinic_address);
        clinicName = findViewById(R.id.clinic_name);
        clinicList = findViewById(R.id.clinic_list);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void fillDetails() {
        findViewById(R.id.container).setVisibility(View.VISIBLE);
        findViewById(R.id.appointment_button).setEnabled(true);
        name.setText(doctor.getName());
        specialization.setText(doctor.getSpecialization());

        List<ClinicDetails> clinics = doctor.getClinics();
        if (clinics.size() == 1) {
            clinics.get(0).setPrimaryFlag(true);
        }
        Optional<ClinicDetails> matchedClinic = clinics.stream().filter(ClinicDetails::isPrimaryFlag).findFirst();

        if (matchedClinic != null) {
            primaryClinic = matchedClinic.get();
            RecyclerView serviceList = findViewById(R.id.services_list);
            serviceList.setLayoutManager(new LinearLayoutManager(DoctorDetailActivity.this, LinearLayoutManager.VERTICAL, false));
            List<Service> services = primaryClinic.getServices();
            if (services != null && services.size() > 0) {
                findViewById(R.id.services_list_container).setVisibility(View.VISIBLE);
                DoctorServiceAdapter doctorServiceAdapter = new DoctorServiceAdapter(services.subList(0, services.size() > 3 ? 3 : services.size()), R.layout.doctor_service_list_item);
                serviceList.setAdapter(doctorServiceAdapter);
                if (services.size() > 3) {
                    findViewById(R.id.more_services).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.more_services).setVisibility(View.GONE);
                }
            } else {
                findViewById(R.id.services_list_container).setVisibility(View.GONE);
            }
        } else {
            return;
        }
        clinicName.setText(primaryClinic.getName());
        String timeToDisplay = "Naveen has done some Mistakes";
        try {
            Date date1 = Utils.toCalendar(primaryClinic.getStart());
            Date date2 = Utils.toCalendar(primaryClinic.getEnd());
            String startTime = Utils.getParsedTime(date1);
            String endTime = Utils.getParsedTime(date2);
            timeToDisplay = startTime + " - " + endTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (clinics.size() > 1) {
            findViewById(R.id.clinic_list_container).setVisibility(View.VISIBLE);
            clinics = clinics.stream().filter(clinicDetails -> !Objects.equals(clinicDetails.getId(), primaryClinic.getId())).collect(Collectors.toList());
            DoctorClinicAdapter doctorClinicAdapter = new DoctorClinicAdapter(clinics);
            clinicList.setLayoutManager(new LinearLayoutManager(DoctorDetailActivity.this, LinearLayoutManager.VERTICAL, false));
            clinicList.setAdapter(doctorClinicAdapter);
        } else {
            findViewById(R.id.clinic_list_container).setVisibility(View.GONE);
        }
        availableTime.setText(timeToDisplay);
        String address = Utils.parseAddress(primaryClinic.getAddress());
        clinicAddress.setText(address);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class FetchDoctor extends AsyncTask<Void, Void, JSONObject> {
        String url = Constants.API_DOCTOR_BASIC + doctorId;
        ProgressDialog progressDialog;

        public FetchDoctor(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!statusCode.trim().equalsIgnoreCase("200")) {
                    Callable callable = new Callable() {
                        @Override
                        public Object call() throws Exception {
                            fetchDoctor();
                            return null;
                        }
                    };
                    new MySnackBar(findViewById(R.id.doctor_experience), Utils.getErrorMessage(statusCode), "indeterminant", Color.YELLOW, "RETRY", callable);
                    return;
                }
                try {
                    doctor = (Doctor) response.get("response");
                    fillDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                Doctor doctor = RestService.instance().get(url, Doctor.class);
                jsonObject.put("StatusCode", "200");
                jsonObject.put("response", doctor);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Error fetching doctor", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }
}
