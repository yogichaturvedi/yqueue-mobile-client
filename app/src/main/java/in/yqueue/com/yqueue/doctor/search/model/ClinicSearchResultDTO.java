package in.yqueue.com.yqueue.doctor.search.model;

/**
 * Created by Yogesh Chaturvedi on 06-06-2018.
 */

public class ClinicSearchResultDTO {
    private String clinicName;
    private String locality;
    private String id;
    private String image;

    /**
     * @return the clinicName
     */
    public String getClinicName() {
        return clinicName;
    }

    /**
     * @param clinicName the clinicName to set
     */
    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    /**
     * @return the locality
     */
    public String getLocality() {
        return locality;
    }

    /**
     * @param locality the locality to set
     */
    public void setLocality(String locality) {
        this.locality = locality;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }
}

