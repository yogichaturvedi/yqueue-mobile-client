package in.yqueue.com.yqueue.common;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.Callable;

/**
 * Created by Yogesh Chaturvedi on 20-04-2018.
 */

public class MySnackBar {
    Callable function;
    boolean showActionButton;
    int actionTextColor = 0, textColor = 0;
    private View layout;
    private String message;
    private String length;
    private String actionButtonText;

    public MySnackBar(View layout, String message, String length) {
        this.layout = layout;
        this.message = message;
        this.length = length;
        init();
    }

    public MySnackBar(View layout, String message, String length, int textColor) {
        this.layout = layout;
        this.message = message;
        this.length = length;
        this.textColor = textColor;
        init();
    }


    public MySnackBar(View layout, String message, String length, String actionButtonText, Callable function) {
        this.layout = layout;
        this.message = message;
        this.length = length;
        this.actionButtonText = actionButtonText;
        this.function = function;
        init();
    }

    public MySnackBar(View layout, String message, String length, int textColor, String actionButtonText, Callable function) {
        this.layout = layout;
        this.message = message;
        this.textColor = textColor;
        this.length = length;
        this.actionButtonText = actionButtonText;
        this.function = function;
        init();
    }

    public MySnackBar(View layout, String message, String length, String actionButtonText, Callable function, int actionTextColor) {
        this.layout = layout;
        this.message = message;
        this.length = length;
        this.actionButtonText = actionButtonText;
        this.function = function;
        init();
    }

    private void init() {
        int snackBarLength = Snackbar.LENGTH_SHORT;
        if (length.equalsIgnoreCase("long")) {
            snackBarLength = Snackbar.LENGTH_LONG;
        } else if (length.equalsIgnoreCase("indeterminant")) {
            snackBarLength = Snackbar.LENGTH_INDEFINITE;
        }
        Snackbar snackbar = Snackbar
                .make(this.layout, this.message, snackBarLength);
        if (actionButtonText != null) {
            snackbar.setAction(actionButtonText, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (function != null) {
                            function.call();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        if (actionTextColor != 0) {
            snackbar.setActionTextColor(actionTextColor);
        }
        if (textColor != 0) {
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
        }

        snackbar.show();
    }
}
