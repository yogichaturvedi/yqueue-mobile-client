package in.yqueue.com.yqueue.doctor.search;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.doctor.DoctorListActivity;
import in.yqueue.com.yqueue.service.Move;

/**
 * Created by Yogesh Chaturvedi on 06-06-2018.
 */

public class SearchSuggestionsAdapter extends RecyclerView.Adapter<SearchSuggestionsAdapter.ViewHolder> {
    private String[] suggestions;

    public SearchSuggestionsAdapter(String[] suggestions) {
        this.suggestions = suggestions;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SearchSuggestionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_suggestion_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, suggestions);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        viewHolder.name.setText(suggestions[position]);
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return suggestions.length;
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final String[] suggestions;
        public TextView name;

        public ViewHolder(View itemLayoutView, String[] suggestions) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.suggestion);
            this.suggestions = suggestions;
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putString("type", Constants.TYPE_SPECIALIZATION);
            bundle.putString("keyword", suggestions[getAdapterPosition()]);
            new Move().activity(view.getContext().getApplicationContext(), DoctorListActivity.class, bundle, "search");
        }
    }
}
