package in.yqueue.com.yqueue.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import in.yqueue.com.yqueue.MainActivity;
import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.common.MySnackBar;
import in.yqueue.com.yqueue.service.Move;
import in.yqueue.com.yqueue.util.Utils;

public class Login extends AppCompatActivity implements
        View.OnClickListener {

    private static final String TAG = "PhoneAuthActivity";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;
    private static final String DEFAULT_COUNTRY_CODE = "91";

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;


    private EditText mPhoneNumberField;

    private Button mStartButton;
    private Button mVerifyButton;
    private TextView mResendButton;
    private EditText mVerificationField;
    private Handler handler;

    private ProgressBar progressBar;
    private Move move;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FirebaseApp.initializeApp(getApplicationContext());
        progressBar = new ProgressBar(this);

        //Initialize move object
        move = new Move();

        //Bind EditText and Button Fields
        mPhoneNumberField = findViewById(R.id.mobile_number);
        mStartButton = findViewById(R.id.button_continue);
        mVerifyButton = findViewById(R.id.button_verify);
        mResendButton = findViewById(R.id.button_resend);
        mVerificationField = findViewById(R.id.verification_code);

        //Bind Click Listeners
        mStartButton.setOnClickListener(this);
        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);


        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                hideProgressBar();
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.

                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                String phone = mPhoneNumberField.getText().toString();
                if (phone != null) {
                    login(phone, DEFAULT_COUNTRY_CODE);
                    mVerificationField.setText(credential.getSmsCode());
                }
//                updateUI(STATE_VERIFY_SUCCESS, credential);
//                // [END_EXCLUDE]
//                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                hideProgressBar();
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    mPhoneNumberField.setError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

                // Show a message and update the UI
                // [START_EXCLUDE]
                updateUI(STATE_VERIFY_FAILED);
                // [END_EXCLUDE]
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                hideProgressBar();
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);
                Snackbar.make(findViewById(android.R.id.content), "Code has been sent to : " + mPhoneNumberField.getText().toString(),
                        Snackbar.LENGTH_SHORT).show();
                RelativeLayout verificationCodeContainer = findViewById(R.id.verification_container);
                RelativeLayout mobileNumberContainer = findViewById(R.id.sigin_container);
                mobileNumberContainer.setVisibility(View.GONE);
                verificationCodeContainer.setVisibility(View.VISIBLE);
                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // [START_EXCLUDE]
                // Update UI
                updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
                mResendButton.setEnabled(false);

                final CountDownTimer resendCodeTimer = new CountDownTimer(10000, 1000) {

                    public void onTick(long millisUntilFinished) {

                        mResendButton.setText("Resend in: " + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        mResendButton.setText("Resend");
                        mResendButton.setEnabled(true);
                        this.cancel();
                    }
                };
                resendCodeTimer.start();
            }
        };
        // [END phone_auth_callbacks]
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        /*FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
        }
        updateUI(currentUser);

        // [START_EXCLUDE]
        if (mVerificationInProgress && validatePhoneNumber()) {
            startPhoneNumberVerification(mPhoneNumberField.getText().toString());
        }*/
        // [END_EXCLUDE]
    }
    // [END on_start_check_user]

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        //Add Country Code default +91
        phoneNumber = "+" + DEFAULT_COUNTRY_CODE + phoneNumber;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();

                            login(user.getPhoneNumber().substring(3), user.getPhoneNumber().substring(1, 3));
                            // [END_EXCLUDE]
                        } else {
                            hideProgressBar();
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                mVerificationField.setError("Invalid code.");
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            updateUI(STATE_SIGNIN_FAILED);
                            // [END_EXCLUDE]
                        }
                    }
                });
    }
    // [END sign_in_with_phone]

    private void signOut() {
        mAuth.signOut();
        updateUI(STATE_INITIALIZED);
    }

    private void updateUI(int uiState) {
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    private void updateUI(int uiState, FirebaseUser user) {

    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {

    }

    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
        }
    }

    private void disableViews(View... views) {
        for (View v : views) {
            v.setEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_continue:
                if (!validatePhoneNumber()) {
                    return;
                }
                showProgressbar();
                startPhoneNumberVerification(mPhoneNumberField.getText().toString());
                break;
            case R.id.button_verify:
                showProgressbar();
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }
                verifyPhoneNumberWithCode(mVerificationId, code);
                break;
            case R.id.button_resend:
                showProgressbar();
                resendVerificationCode("+" + DEFAULT_COUNTRY_CODE + mPhoneNumberField.getText().toString(), mResendToken);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void showProgressbar() {
        findViewById(R.id.login_progress_bar).setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideProgressBar() {
        findViewById(R.id.login_progress_bar).setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void login(String phoneNumber, String countryCode) {

        new MySnackBar(mPhoneNumberField, "User logged in with phone number " + phoneNumber, "short");
        //TODO: Check if user already registerd or not


        ProgressDialog loginProgressDialog = new MyProgressDialog().init(Login.this, "", "", true);

        final AsyncTask<Void, Void, JSONObject> loginTask = new GetPatient(loginProgressDialog, phoneNumber, countryCode).execute();
        loginProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                loginTask.cancel(true);
            }
        });

    }

    class GetPatient extends AsyncTask<Void, Void, JSONObject> {
        String url = Constants.API_PATIENT;
        ProgressDialog progressDialog;
        private String contactNumber;
        private String countryCode;

        public GetPatient(ProgressDialog progressDialog, String contactNumber, String countryCode) {
            this.contactNumber = contactNumber;
            this.countryCode = countryCode;
            this.url = this.url + "is-exist?phoneNo=" + contactNumber + "&countryCode=" + countryCode;
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!statusCode.trim().equalsIgnoreCase("200") && !statusCode.trim().equalsIgnoreCase("303")) {
                    new MySnackBar(findViewById(android.R.id.content), Utils.getErrorMessage(statusCode), "long", Color.RED);
                    return;
                }

                //Patient logged in for first time move to profile edit page
                if (statusCode.trim().equalsIgnoreCase("303")) {
                    User user = new User();
                    user.setContactNumber(contactNumber);
                    user.setCountryCode(countryCode);
                    hideProgressBar();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user", user);
                    move.activity(Login.this, UserProfileEdit.class, bundle, "user");
                }
                //Patient exists already move to home screen
                else {
                    try {
                        User user = (User) response.get("response");
                        Intent intent = new Intent(Login.this, MainActivity.class);
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        try {
                            editor.putString(String.valueOf(R.string.userSP_user_key), new ObjectMapper().writeValueAsString(user));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        editor.apply();
                        hideProgressBar();
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                User user = RestService.instance().get(url, User.class);
                if (user == null) {
                    jsonObject.put("StatusCode", "303");
                    return jsonObject;
                }
                jsonObject.put("StatusCode", "200");
                jsonObject.put("response", user);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Error in saving patient", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }
}


