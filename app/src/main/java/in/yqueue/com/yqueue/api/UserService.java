package in.yqueue.com.yqueue.api;

import android.app.ProgressDialog;
import android.content.Context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.concurrent.ExecutionException;

import in.yqueue.com.yqueue.exceptions.JsonException;
import in.yqueue.com.yqueue.exceptions.NoInternetException;
import in.yqueue.com.yqueue.user.User;

/**
 * Created by Yogesh on 22-07-2016.
 */
public class UserService {
    private String endpoint = "http://54.186.111.254:9090/yqueue/api/v1/patient/";


    public User SignIn(Context context, String contactNumber, String countryCode) throws Exception {
        String url = endpoint + "is-exist?phoneNo=" + contactNumber + "&countryCode=" + countryCode;
        try {
            User response = new HttpGet<User>(context, url, User.class).execute().get();
            return response;
//            return new ObjectMapperService().readValue(response, User.class);
        } catch (InterruptedException e) {
            throw e;
        } catch (ExecutionException e) {
            throw e;
        }
    }

//    public String userForgotPassword(Context context, String email, String url) {
//        try {
//            return new HttpGet<String>(context, url, String.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }

    public String userSignUp(Context context, ProgressDialog saveUserProgressDialog, User user) {
        String url = endpoint;
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("name", name);
//            jsonObject.put("email", email);
//            jsonObject.put("password", password);
//            jsonObject.put("contactNumber", contact);
//            jsonObject.put("image", img_str);
//        } catch (JSONException e) {
//            throw new JsonException();
//        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = null;
        try {
            entity = new HttpEntity<String>(new ObjectMapper().writeValueAsString(user), headers);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        try {
            return new HttpPost<String>(context, saveUserProgressDialog,url, entity, String.class).execute().get();
        } catch (InterruptedException e) {
            throw new NoInternetException();
        } catch (ExecutionException e) {
            throw new NoInternetException();
        }
    }

//    public User FetchUserDetails(Context context, String url) {
//
//
//        try {
//            return new HttpGet<User>(context, url, User.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }

    public String userUpdateDetails(Context context, String name, String email, String password, String contact, String image_str, String url) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("password", password);
            jsonObject.put("contact", contact);
            jsonObject.put("image", image_str);
        } catch (JSONException e) {
            throw new JsonException();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("cookie", "XDEBUG_SESSION=16854");
        HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
        try {
            new HttpPut(context, url, entity).execute().get();
            return "saved user successfully";
        } catch (InterruptedException e) {
            throw new NoInternetException();
        } catch (ExecutionException e) {
            throw new NoInternetException();
        }
    }
}

