package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh Chaturvedi on 27-04-2018.
 */

public class Service{
    private String id;
    private String name;

    public Service(@JsonProperty("id") String id,
                   @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
