package in.yqueue.com.yqueue.user.appointments.liveStatus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

import in.yqueue.com.yqueue.R;

public class AppointmentLiveStatusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appointment_live_status_activity);

        //Toolbar setup and title
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Dr Amit Sirohiya");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Setting live status list
        AppointmentLiveStatusListAdapter liveStatusListAdapter = new AppointmentLiveStatusListAdapter(getLiveStatusList());
        RecyclerView recyclerView = findViewById(R.id.live_status_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(liveStatusListAdapter);
    }

    private ArrayList<LiveStatusPojo> getLiveStatusList() {
        ArrayList<LiveStatusPojo> liveStatusList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            LiveStatusPojo liveStatus = new LiveStatusPojo(i, "Doctor is consulting with token", true, "5:10 PM");
            liveStatusList.add(liveStatus);
        }
        return liveStatusList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
