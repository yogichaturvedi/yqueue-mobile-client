package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class Doctor {
    private String id;
    private String name;
    private String specialization;
    private String image;
    private int fees;
    private float experience;
    private List<ClinicDetails> clinics;

    public Doctor(@JsonProperty("id") String id,
                  @JsonProperty("name") String name,
                  @JsonProperty("specialization") String specialization,
                  @JsonProperty("fees") int fees,
                  @JsonProperty("image") String image,
                  @JsonProperty("experience") float experience,
                  @JsonProperty("clinics") List<ClinicDetails> clinics) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
        this.image = image;
        this.fees = fees;
        this.experience = experience;
        this.clinics = clinics;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getFees() {
        return fees;
    }

    public void setFees(int fees) {
        this.fees = fees;
    }

    public float getExperience() {
        return experience;
    }

    public void setExperience(float experience) {
        this.experience = experience;
    }

    public List<ClinicDetails> getClinics() {
        return clinics;
    }

    public void setClinics(List<ClinicDetails> clinics) {
        this.clinics = clinics;
    }
}