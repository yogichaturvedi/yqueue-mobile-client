package in.yqueue.com.yqueue.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import in.yqueue.com.yqueue.MainActivity;
import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.service.Move;

/**
 * Created by Yogesh Chaturvedi on 24-03-2018.
 */

public class Profile extends Fragment implements View.OnClickListener {
    private Move move;
    private User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_profile, container, false);

        //Initializing firebase app
        FirebaseApp.initializeApp(getContext());

        //Fetching User from shared preferences
        try {
            user = new ObjectMapper().readValue(getActivity().getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE).getString(String.valueOf(R.string.userSP_user_key), ""), User.class);
            fillDetails(view, user);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Initialize Move activity

        move = new Move();

        //Set on click listener on logout button
        view.findViewById(R.id.logout).setOnClickListener(this);

        //Set on click listener on edit profile button
        view.findViewById(R.id.profile_edit).setOnClickListener(this);

        return view;
    }

    private void fillDetails(View view, User user) {
        TextView patientName = view.findViewById(R.id.patient_name);
        TextView patientAge = view.findViewById(R.id.patient_age);
        ImageView patientGender = view.findViewById(R.id.patient_gender);
        ImageView patientImage = view.findViewById(R.id.user_image);
        TextView patientContactNumber = view.findViewById(R.id.patient_contact_number);
        TextView patientEmail = view.findViewById(R.id.patient_email);

        patientName.setText(user.getName());
        patientAge.setText(String.valueOf(user.getAge()));
        String contactNumberToDisplay = "+" + user.getCountryCode() + "-" + user.getContactNumber();
        patientContactNumber.setText(contactNumberToDisplay);
        patientEmail.setText(user.getEmail());
        if (user.getImage() != null && !user.getImage().equals("")) {
            patientImage.setImageURI(null);
            Picasso.Builder picassoBuilder = new Picasso.Builder(getActivity().getApplicationContext());
            Picasso picasso = picassoBuilder.build();
            picasso.setIndicatorsEnabled(true);
            picasso.load(Constants.API_IMAGE_PATIENT + user.getImage()).networkPolicy(NetworkPolicy.NO_CACHE).placeholder(R.drawable.progress_animation).error(R.drawable.user_no_image).into(patientImage);
        } else {
            patientImage.setImageURI(null);
            patientImage.setImageResource(R.drawable.user_no_image);
        }
        if (user.getGender().equalsIgnoreCase("female")) {
            patientGender.setImageResource(R.drawable.icon_female);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logout:
                final FirebaseAuth mAuth = FirebaseAuth.getInstance();
                getActivity().getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE).edit().clear().apply();
                mAuth.signOut();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                break;
            case R.id.profile_edit:
                Bundle bundle = new Bundle();
                move.activity(getActivity(), UserProfileEdit.class);
                break;
        }
    }
}
