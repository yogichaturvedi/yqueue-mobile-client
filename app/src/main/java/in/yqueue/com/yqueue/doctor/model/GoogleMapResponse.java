package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Yogesh Chaturvedi on 31-05-2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleMapResponse {

    private List<String> destination_addresses;
    private List<String> origin_addresses;
    private List<Rows> rows;

    public void setDestination_addresses(List<String> destination_addresses) {
        this.destination_addresses = destination_addresses;
    }

    public void setOrigin_addresses(List<String> origin_addresses) {
        this.origin_addresses = origin_addresses;
    }

    public void setRows(List<Rows> rows) {
        this.rows = rows;
    }

    public List<String> getDestination_addresses() {
        return destination_addresses;
    }

    public List<String> getOrigin_addresses() {
        return origin_addresses;
    }

    public List<Rows> getRows() {
        return rows;
    }


//getters
}

