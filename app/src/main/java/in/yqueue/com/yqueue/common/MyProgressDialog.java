package in.yqueue.com.yqueue.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.view.WindowManager;

import java.util.concurrent.Callable;

import in.yqueue.com.yqueue.R;

/**
 * Created by Yogesh Chaturvedi on 21-04-2018.
 */

public class MyProgressDialog {

    public ProgressDialog init(Activity activity, String title, String message, boolean cancelable) {
        ProgressDialog progress = new ProgressDialog(activity, R.style.CustomProgressBarStyle);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage(message);
        progress.setTitle(title);
        // progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setCancelable(cancelable);
        progress.setProgressStyle(android.R.style.Widget_Holo_Light_ProgressBar_Small);
        progress.setCanceledOnTouchOutside(false);
        return progress;
    }

    public ProgressDialog init(Activity activity, String title, String message, boolean cancelable, final Callable callable) {
        ProgressDialog progress = new ProgressDialog(activity, R.style.CustomProgressBarStyle);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage(message);
        progress.setTitle(title);
        // progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setCancelable(cancelable);
        progress.setProgressStyle(android.R.style.Widget_Holo_Light_ProgressBar_Small);
        progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                try {
                    callable.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        progress.setCanceledOnTouchOutside(false);
        return progress;
    }

    public void show(ProgressDialog progress) {
        if (progress != null && !progress.isShowing()) {
            progress.show();
        }
    }

    public void hide(ProgressDialog progress) {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }
}
