package in.yqueue.com.yqueue;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Yogesh Chaturvedi on 31-05-2018.
 */

public class LocationModel implements Serializable {
    @JsonProperty double latitude;
    @JsonProperty double longitude;

    public LocationModel() {
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
