//package in.yqueue.com.yqueue.api;
//
//import android.content.Context;
//
//import com.example.yogesh.ecart.R;
//import com.okruti.ecart.exceptions.NoInternetException;
//import com.okruti.ecart.model.CartItem;
//
//import java.util.concurrent.ExecutionException;
//
///**
// * Created by Yogesh on 08-07-2016.
// */
//public class CartService {
//    public String cartItemCount(Context context) {
//
//        UserService userService = new UserService();
//        String url = context.getString(R.string.local)
//                + context.getString(R.string.user)
//                + userService.getUserId(context)
//                + context.getString(R.string.user_cart_item_count);
//        try {
//            return new HttpGet<String>(context, url, String.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public CartItem[] loadCartItems(Context context, String url) {
//        try {
//            return new HttpGet<CartItem[]>(context, url, CartItem[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public String loadPayentDetails(Context context, String url) {
//        try {
//            return new HttpGet<String>(context, url, String.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public String updateQuantity(Context context, String url) {
//        try {
//            return new HttpGet<String>(context, url, String.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public String removeCartItem(Context context, String url) {
//        try {
//            return new HttpGet<String>(context, url, String.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//}
