package in.yqueue.com.yqueue.exceptions;

/**
 * Created by Yogesh on 27-07-2016.
 */
public class NoInternetException extends EcartException  {

    private  String message;

    public NoInternetException() {
         message ="No Internet";
    }

    public NoInternetException(String msg) {
         this.message =msg;
    }


}
