package in.yqueue.com.yqueue.book.timeSlot;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import in.yqueue.com.yqueue.MainActivity;
import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.book.BookDateActivity;
import in.yqueue.com.yqueue.book.timeSlot.model.TimeGroup;
import in.yqueue.com.yqueue.book.timeSlot.model.TimeSlots;
import in.yqueue.com.yqueue.common.DialogBoxFrag;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.common.MySnackBar;
import in.yqueue.com.yqueue.service.Move;
import in.yqueue.com.yqueue.user.appointments.model.Book;
import in.yqueue.com.yqueue.util.Utils;

public class TimeSlotAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<TimeGroup> timeGroups;
    private String timeSlotId;
    private String patientId;
    private String doctorId;
    private String clinicId;
    private String date;

    public TimeSlotAdapter(Context context, ArrayList<TimeGroup> timeGroups, String patientId, String doctorId, String clinicId) {
        this.context = context;
        this.timeGroups = timeGroups;
        this.patientId = patientId;
        this.doctorId = doctorId;
        this.clinicId = clinicId;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<TimeSlots> timeSlots = timeGroups.get(groupPosition).getTimeSlots();
        return timeSlots.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        TimeSlots timeSlots = (TimeSlots) getChild(groupPosition, childPosition);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.book_time_slot_child_items, null);
        }

        final TextView timeSlot = (TextView) view.findViewById(R.id.child_time_slot);

        try {
            Date date1 = Utils.toCalendar(timeSlots.getStart());
            Date date2 = Utils.toCalendar(timeSlots.getEnd());
            String startTime = Utils.getParsedTime(date1);
            String endTime = Utils.getParsedTime(date2);
            String timeToDisplay = startTime + " - " + endTime;
            timeSlot.setText(timeToDisplay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        timeSlot.setText(timeSlots.getLabel());
        TextView status = (TextView) view.findViewById(R.id.status);
        status.setText(timeSlots.isAvailable() ? "Available" : "Filled");
        if (timeSlots.isAvailable()) {
            status.setTextColor(Color.parseColor("#0cae0c"));
        } else {
            status.setTextColor(Color.parseColor("#FF0000"));
        }
        if (isLastChild) {
            View divider = view.findViewById(R.id.divider);
            divider.setVisibility(View.VISIBLE);
        } else {
            View divider = view.findViewById(R.id.divider);
            divider.setVisibility(View.GONE);
        }

        Button bookSlot = (Button) view.findViewById(R.id.book_time_slot);

        if (!timeSlots.isAvailable()) {
            bookSlot.setVisibility(View.GONE);
        }

        bookSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Callable<Void> positiveFunction = new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        timeSlotId = timeSlots.getId();
                        date = timeSlots.getStart();
                        bookAppointment();
                        return null;
                    }
                };
                new DialogBoxFrag(view.getContext(),
                        timeSlot.getText().toString(),
                        "Do you really want to book appointment for falana doctor?",
                        R.drawable.doctor_small_icon,
                        true,
                        "Confirm",
                        "Cancel",
                        positiveFunction,
                        null);
            }
        });

        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        List<TimeSlots> timeSlots = timeGroups.get(groupPosition).getTimeSlots();
        return timeSlots.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return timeGroups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return timeGroups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        TimeGroup timeGroup = (TimeGroup) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.book_time_slots_group_items, null);
        }

        TextView timeSlot = (TextView) view.findViewById(R.id.group_time_slot);

        Date date1 = null;

        try {
            date1 = Utils.toCalendar(timeGroup.getStart());
            Date date2 = Utils.toCalendar(timeGroup.getEnd());
            String startTime = Utils.getParsedTime(date1);
            String endTime = Utils.getParsedTime(date2);
            String timeToDisplay = startTime + " - " + endTime;
            timeSlot.setText(timeToDisplay);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        timeSlot.setText(timeGroup.getLabel());
        if (isLastChild) {
            View dividerLarge = view.findViewById(R.id.divider_large);
            dividerLarge.setVisibility(View.GONE);
            View dividerSmall = view.findViewById(R.id.divider_small);
            dividerSmall.setVisibility(View.VISIBLE);
        } else {
            View dividerLarge = view.findViewById(R.id.divider_large);
            dividerLarge.setVisibility(View.VISIBLE);
            View dividerSmall = view.findViewById(R.id.divider_small);
            dividerSmall.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void bookAppointment() {
        if (doctorId != null && clinicId != null && date != null && patientId != null) {
            ProgressDialog fetchTimeSlotProgressDialog = new MyProgressDialog().init(((BookDateActivity) context), "", "", true);
            new BookAppointment(fetchTimeSlotProgressDialog).execute();
        }
    }


    private void moveToAppointmentDetail() {
        Bundle bundle = new Bundle();
        bundle.putString("fragmentName", "appointments");
        new Move().activity(context, MainActivity.class, bundle, "fragmentName");
    }

    class BookAppointment extends AsyncTask<Void, Void, JSONObject> {
        String url = Constants.API_APPOINTMENT_BOOK;
        ProgressDialog progressDialog;

        public BookAppointment(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!statusCode.trim().equalsIgnoreCase("201")) {
                    Callable callable = new Callable() {
                        @Override
                        public Object call() throws Exception {
                            bookAppointment();
                            return null;
                        }
                    };
                    new MySnackBar(((BookDateActivity) context).findViewById(R.id.book_tomorrow_expandable_list), Utils.getErrorMessage(statusCode), "indeterminant", Color.YELLOW, "RETRY", callable);
                    return;
                }
                moveToAppointmentDetail();
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                Book book = new Book();
                book.setClinicId(clinicId);
                book.setDate(date);
                book.setDoctorId(doctorId);
                book.setPatientId(patientId);
                book.setTimeSlotId(timeSlotId);
                JSONObject response = RestService.instance().post(url, book, JSONObject.class);
                jsonObject.put("StatusCode", "201");
                jsonObject.put("response", response);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Error fetching doctor", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }
}