//package in.yqueue.com.yqueue.api;
//
//import android.content.Context;
//
//import com.example.yogesh.ecart.R;
//import com.okruti.ecart.exceptions.NoInternetException;
//import com.okruti.ecart.model.Product;
//import com.okruti.ecart.model.Search;
//
//import java.util.concurrent.ExecutionException;
//
///**
// * Created by Yogesh on 26-07-2016.
// */
//public class SearchService {
//    public Search[] searchSuggestion(Context context, String query) {
//        String url = context.getString(R.string.local) + context.getString(R.string.search) + "/" + query;
//        try {
//            return new HttpGet<Search[]>(context, url, Search[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public Product[] submitQueryForProducts(Context context, String query) {
//        String url = context.getString(R.string.local) + context.getString(R.string.search)
//                + "/query/" + query;
//        try {
//            return new HttpGet<Product[]>(context, url, Product[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public Product submitQueryForProductDetail(Context context, String query) {
//        String url = context.getString(R.string.local) + context.getString(R.string.search)
//                + "/query/" + query;
//        try {
//            return new HttpGet<Product>(context, url, Product.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public void submitDetailedQuery(Context context, String query) {
//        String url = context.getString(R.string.local) + context.getString(R.string.search) + "/" + query;
//        HttpGetHeadersService httpGetHeadersService = new HttpGetHeadersService();
//        httpGetHeadersService.getHeaders(url);
//    }
//}