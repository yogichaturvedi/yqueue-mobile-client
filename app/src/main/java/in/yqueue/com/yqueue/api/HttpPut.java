package in.yqueue.com.yqueue.api;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by Yogesh on 26-07-2016.
 */
class HttpPut extends AsyncTask<Void, Void, Object> {
    private String url;
    Object request;
    private Context context;

    public HttpPut(Context context, String url, Object request) {
        this.context = context;
        this.url = url;
        this.request = request;
    }

    @Override
    protected Object doInBackground(Void... params) {
        RestService.instance().put(this.url, request);
        return null;
    }
}
