package in.yqueue.com.yqueue.user;

import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Yogesh Chaturvedi on 24-03-2018.
 */

public class User implements Serializable{
    @JsonProperty
    private String id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String contactNumber;
    @JsonProperty
    private String countryCode;
    @JsonProperty
    private String email;
    @JsonProperty
    private String image;
    @JsonProperty
    private String gender;
    @JsonProperty
    private int age;

//    User(@JsonProperty int id, @JsonProperty String name, @JsonProperty String contactNumber, @JsonProperty String countryCode, @JsonProperty String email, @JsonProperty String image, @JsonProperty String gender, @JsonProperty int age) {
//        this.id = id;
//        this.name = name;
//        this.contactNumber = contactNumber;
//        this.countryCode = countryCode;
//        this.email = email;
//        this.image = image;
//        this.gender = gender;
//        this.age = age;
//    }
//
//    User(@JsonProperty String name, @JsonProperty String contactNumber, @JsonProperty String countryCode, @JsonProperty String email, @JsonProperty String image, @JsonProperty String gender, @JsonProperty int age) {
//        this.name = name;
//        this.contactNumber = contactNumber;
//        this.countryCode = countryCode;
//        this.email = email;
//        this.image = image;
//        this.gender = gender;
//        this.age = age;
//    }

    User() {

    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
