package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh Chaturvedi on 27-04-2018.
 */

public class ContactInfo {
    private String id;
    private String contactNumber;

    public ContactInfo(@JsonProperty("id") String id, @JsonProperty("contactNumber") String contactNumber) {
        this.id = id;
        this.contactNumber = contactNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}