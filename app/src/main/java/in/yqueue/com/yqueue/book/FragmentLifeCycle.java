package in.yqueue.com.yqueue.book;

/**
 * Created by Naveen Kumawat on 18-04-2018.
 */

interface FragmentLifecycle {

    public void onPauseFragment();
    public void onResumeFragment();

}
