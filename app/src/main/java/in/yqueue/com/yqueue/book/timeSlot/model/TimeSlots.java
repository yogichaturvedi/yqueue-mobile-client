package in.yqueue.com.yqueue.book.timeSlot.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh Chaturvedi on 28-04-2018.
 */

public class TimeSlots {
    private String id;
    private String label;
    private String start;
    private String end;
    private int occupied;
    private boolean isAvailable;

    public TimeSlots(@JsonProperty("id") String id,
                     @JsonProperty("label") String label,
                     @JsonProperty("start") String start,
                     @JsonProperty("end") String end,
                     @JsonProperty("occupied") int occupied,
                     @JsonProperty("isAvailable") boolean isAvailable) {
        this.id = id;
        this.label = label;
        this.start = start;
        this.end = end;
        this.occupied = occupied;
        this.isAvailable = isAvailable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getOccupied() {
        return occupied;
    }

    public void setOccupied(int occupied) {
        this.occupied = occupied;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }
}
