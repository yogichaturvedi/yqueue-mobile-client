package in.yqueue.com.yqueue.doctor.search;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.doctor.DoctorListActivity;
import in.yqueue.com.yqueue.doctor.search.model.ClinicSearchResultDTO;
import in.yqueue.com.yqueue.doctor.search.model.SearchModel;
import in.yqueue.com.yqueue.service.Move;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class ClinicSearchAdapter extends RecyclerView.Adapter<ClinicSearchAdapter.ViewHolder> {
    private List<ClinicSearchResultDTO> clinicSearchList;

    public ClinicSearchAdapter(List<ClinicSearchResultDTO> clinicSearchList) {
        this.clinicSearchList = clinicSearchList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ClinicSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clinic_search_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, clinicSearchList);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        ClinicSearchResultDTO doctorBasicDetail = clinicSearchList.get(position);
        viewHolder.name.setText(doctorBasicDetail.getClinicName());
        viewHolder.location.setText(doctorBasicDetail.getLocality());
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return clinicSearchList.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final List<ClinicSearchResultDTO> clinicSearchResultDTOS;
        public TextView name;
        public TextView location;

        public ViewHolder(View itemLayoutView, List<ClinicSearchResultDTO> clinicSearchResultDTOS) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.clinic_name);
            location = (TextView) itemLayoutView.findViewById(R.id.clinic_location);
            this.clinicSearchResultDTOS = clinicSearchResultDTOS;
            itemLayoutView.setOnClickListener(this);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            SharedPreferences sharedPreferences = view.getContext().getSharedPreferences(view.getContext().getString(R.string.search_suggestion), Context.MODE_PRIVATE);
            String searchSuggestionString = sharedPreferences.getString(view.getContext().getString(R.string.search_suggestion), null);
            SearchModel searchModel;
            if (searchSuggestionString == null) {
                searchModel = new SearchModel();
            } else {
                try {
                    searchModel = new ObjectMapper().readValue(searchSuggestionString, SearchModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    searchModel = new SearchModel();
                }
            }
            try {
                List<ClinicSearchResultDTO> clinicSearchResults = searchModel.getClinicSearchResults();
                ClinicSearchResultDTO itemToAdd = clinicSearchResultDTOS.get(getAdapterPosition());
                boolean isExists = clinicSearchResults.stream().anyMatch(e -> e.getId().equalsIgnoreCase(itemToAdd.getId()));
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if (isExists) {
                    clinicSearchResults.removeIf(e -> e.getId().equalsIgnoreCase(itemToAdd.getId()));
                    clinicSearchResults.add(itemToAdd);
                } else {
                    clinicSearchResults.add(itemToAdd);
                    if (clinicSearchResults.size() > 3) {
                        clinicSearchResults.remove(clinicSearchResults.get(0));
                    }
                }
                editor.putString(view.getContext().getString(R.string.search_suggestion), new ObjectMapper().writeValueAsString(searchModel));
                editor.apply();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bundle bundle = new Bundle();
            bundle.putString("type", Constants.TYPE_CLINIC);
            bundle.putString("keyword", clinicSearchResultDTOS.get(getAdapterPosition()).getClinicName());
            new Move().activity(view.getContext().getApplicationContext(), DoctorListActivity.class, bundle, "search");
        }
    }

}
