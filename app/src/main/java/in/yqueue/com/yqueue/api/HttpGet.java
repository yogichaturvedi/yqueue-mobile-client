package in.yqueue.com.yqueue.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import in.yqueue.com.yqueue.user.User;

/**
 * Created by Yogesh on 26-07-2016.
 */
public class HttpGet<T> extends AsyncTask<Void, Void, T> {
    private String url;
    private Class<T> responseType;
    private Context context;
    public ProgressDialog progressDialog;

    public HttpGet(Context context, String url, Class<T> responseType) {
        this.context = context ;
        this.url = url;
        this.responseType = responseType;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        showProgressDialog(context);
    }

    @Override
    protected void onPostExecute(T t) {
        super.onPostExecute(t);
//        hideProgressDialog();
    }

    @Override
    protected T doInBackground(Void... params) {
        return RestService.instance().get(url, responseType);
    }

    private void showProgressDialog(Context context) {
    }

    private void hideProgressDialog() {
    }
}
