package in.yqueue.com.yqueue.doctor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.doctor.search.DoctorSearchActivity;
import in.yqueue.com.yqueue.service.Move;

/**
 * Created by Yogesh Chaturvedi on 24-04-2018.
 */

public class HomePageFragment extends Fragment implements View.OnClickListener{
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_page_fragment, container, false);

        view.findViewById(R.id.book_appointment).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.book_appointment:
                new Move().activity(getActivity(),DoctorSearchActivity.class);
                break;
        }
    }
}
