package in.yqueue.com.yqueue.user.appointments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.yqueue.com.yqueue.R;

public class AppointmentListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.user_appointment_list_activity, container, false);
//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        toolbar.setTitle("Your Appointments");
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppointmentListAdapter upcomingAppointmentListAdapter = new AppointmentListAdapter(getUpcomingAppointmentList());
        RecyclerView upcomingRecyclerView = view.findViewById(R.id.upcomming_appointment_list);
        upcomingRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        upcomingRecyclerView.setAdapter(upcomingAppointmentListAdapter);

        AppointmentListAdapter pastAppointmentListAdapter = new AppointmentListAdapter(getPastAppointmentList());
        RecyclerView pastAppointmentRecyclerView = view.findViewById(R.id.past_appointment_list);
        pastAppointmentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        pastAppointmentRecyclerView.setAdapter(pastAppointmentListAdapter);
        return view;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                // todo: goto back activity from here
//                Intent intent = new Intent(AppointmentListFragment.this, DoctorListActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();
//                return true;
//
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    public ArrayList<UserAppointmentPojo> getUpcomingAppointmentList() {
        ArrayList<UserAppointmentPojo> appointmentLists = new ArrayList<>();
        int i = 0;
        UserAppointmentPojo appointment = new UserAppointmentPojo("Amit Kumar Sirohiya", "7:00 PM", "12 Dec, 2018", "7:00 PM", "Gyneccologist", "Tilawala Janana Hospital");
        while (i <= 3) {
            i += 1;
            appointment.setDoctorName(appointment.getDoctorName());
            appointmentLists.add(appointment);
        }
        return appointmentLists;
    }

    public ArrayList<UserAppointmentPojo> getPastAppointmentList() {
        ArrayList<UserAppointmentPojo> appointmentLists = new ArrayList<>();
        int i = 0;
        UserAppointmentPojo appointment = new UserAppointmentPojo("Yogesh Chaturvedi", "7:00 PM", "12 Dec, 2018", "7:00 PM", "Neurology", "Geetanjali Medical College and Hospital");
        while (i <= 3) {
            i += 1;
            appointment.setDoctorName(appointment.getDoctorName());
            appointmentLists.add(appointment);
        }
        return appointmentLists;
    }
}