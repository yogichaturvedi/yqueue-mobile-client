package in.yqueue.com.yqueue.doctor.search;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.doctor.DoctorDetailActivity;
import in.yqueue.com.yqueue.doctor.search.model.DoctorSearchResultDTO;
import in.yqueue.com.yqueue.doctor.search.model.SearchModel;
import in.yqueue.com.yqueue.service.Move;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class DoctorSearchAdapter extends RecyclerView.Adapter<DoctorSearchAdapter.ViewHolder> {
    private List<DoctorSearchResultDTO> doctorSearchList;

    public DoctorSearchAdapter(List<DoctorSearchResultDTO> doctorSearchList) {
        this.doctorSearchList = doctorSearchList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DoctorSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doctor_search_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, doctorSearchList);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        DoctorSearchResultDTO doctorBasicDetail = doctorSearchList.get(position);
        viewHolder.name.setText("Dr. " + doctorBasicDetail.getName());
        Picasso.get().load(doctorBasicDetail.getImage()).placeholder(R.drawable.doctor_icon).into(viewHolder.image);
        viewHolder.specialization.setText(doctorBasicDetail.getSpecialization());
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return doctorSearchList.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final List<DoctorSearchResultDTO> doctorSearchResultDTOS;
        public TextView name;
        public ImageView image;
        public TextView specialization;

        public ViewHolder(View itemLayoutView, List<DoctorSearchResultDTO> doctorSearchResultDTOS) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.doctor_name);
            image = (ImageView) itemLayoutView.findViewById(R.id.item_image);
            specialization = (TextView) itemLayoutView.findViewById(R.id.doctor_specialization);
            this.doctorSearchResultDTOS = doctorSearchResultDTOS;
            itemLayoutView.setOnClickListener(this);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            SharedPreferences sharedPreferences = view.getContext().getSharedPreferences(view.getContext().getString(R.string.search_suggestion), Context.MODE_PRIVATE);
            String searchSuggestionString = sharedPreferences.getString(view.getContext().getString(R.string.search_suggestion), null);
            SearchModel searchModel;
            if (searchSuggestionString == null) {
                searchModel = new SearchModel();
            } else {
                try {
                    searchModel = new ObjectMapper().readValue(searchSuggestionString, SearchModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    searchModel = new SearchModel();
                }
            }
            DoctorSearchResultDTO itemToAdd = doctorSearchResultDTOS.get(getAdapterPosition());
            try {
                List<DoctorSearchResultDTO> doctorSearchResults = searchModel.getDoctorSearchResults();

                boolean isExists = searchModel.getDoctorSearchResults().stream().anyMatch(e -> e.getId().equalsIgnoreCase(itemToAdd.getId()));
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if (isExists) {
                    doctorSearchResults.removeIf(e -> e.getId().equalsIgnoreCase(itemToAdd.getId()));
                    doctorSearchResults.add(itemToAdd);
                } else {
                    doctorSearchResults.add(itemToAdd);
                    if (doctorSearchResults.size() > 4) {
                        doctorSearchResults.remove(doctorSearchResults.get(0));
                    }
                }
                editor.putString(view.getContext().getString(R.string.search_suggestion), new ObjectMapper().writeValueAsString(searchModel));
                editor.apply();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String name = itemToAdd.getName();
            Bundle bundle = new Bundle();
            String id = itemToAdd.getId();
            bundle.putString("id", id);
            bundle.putString("name", name);
            new Move().activity(view.getContext(), DoctorDetailActivity.class, bundle, "doctor");
        }
    }

}
