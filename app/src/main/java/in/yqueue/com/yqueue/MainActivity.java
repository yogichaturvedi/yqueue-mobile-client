package in.yqueue.com.yqueue;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import devlight.io.library.ntb.NavigationTabBar;
import in.yqueue.com.yqueue.doctor.HomePageFragment;
import in.yqueue.com.yqueue.notification.NotificationListFragment;
import in.yqueue.com.yqueue.service.Move;
import in.yqueue.com.yqueue.user.Login;
import in.yqueue.com.yqueue.user.Profile;
import in.yqueue.com.yqueue.user.appointments.AppointmentListFragment;

public class MainActivity extends AppCompatActivity {

    MenuItem prevMenuItem;
    private FirebaseAuth mAuth;
    private Move move;
    private ViewPager viewPager;
    private Profile profileFragment;
    private HomePageFragment homePageFragment;
    private AppointmentListFragment appointmentListFragment;
    private boolean doubleBackToExitPressedOnce;
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };
    private Handler mHandler = new Handler();
    private int LOCATION_REQUEST_CODE = 1;
    private GPSTracker gps;
    private NotificationListFragment notificationListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize Firebase App
        FirebaseApp.initializeApp(getApplicationContext());

        //Initialize move object
        move = new Move();

        initUI();
        gotoFragment();


    }

    private void initUI() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        //****************View Pager implementation starts************************//
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);
        //****************View Pager implementation ends************************//

        final String[] colors = getResources().getStringArray(R.array.default_preview);

        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.nav_home),
                        Color.parseColor(colors[0]))
                        .title("Home")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.nav_list),
                        Color.parseColor(colors[1]))
                        .title("Appointments")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.nav_bell),
                        Color.parseColor(colors[2]))
                        .title("Notification")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.nav_user),
                        Color.parseColor(colors[3]))
                        .title("Account")
                        .build()
        );
        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 2);

        //IMPORTANT: ENABLE SCROLL BEHAVIOUR IN COORDINATOR LAYOUT
        navigationTabBar.setBehaviorEnabled(true);

        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {
            }

            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                model.hideBadge();
            }
        });
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        homePageFragment = new HomePageFragment();
        appointmentListFragment = new AppointmentListFragment();
        notificationListFragment = new NotificationListFragment();
        profileFragment = new Profile();
        adapter.addFragment(homePageFragment);
        adapter.addFragment(appointmentListFragment);
        adapter.addFragment(notificationListFragment);
        adapter.addFragment(profileFragment);
        viewPager.setAdapter(adapter);
    }

    public void gotoFragment() {
        if (getIntent().hasExtra("fragmentName")) {
            String fragmentName = getIntent().getBundleExtra("fragmentName").getString("fragmentName");
            switch (fragmentName) {
                case "home":
                    viewPager.setCurrentItem(0, true);
                    break;
                case "appointments":
                    viewPager.setCurrentItem(1, true);
                    break;
                case "profile":
                    viewPager.setCurrentItem(3, true);
                    break;
                case "notification":
                    viewPager.setCurrentItem(2, true);
                    break;
                default:
                    break;
            }
        } else {
            viewPager.setCurrentItem(0, true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        String user = getSharedPreferences(String.valueOf(R.string.userSP), Context.MODE_PRIVATE).getString(String.valueOf(R.string.userSP_user_key), null);
        if (user == null) {
//            move.activity(this, Login.class, null, null);
        }
        location();
    }

    private void location() {
        Context mContext = this;
        if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
//            Toast.makeText(mContext, "You have no need for granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(mContext, MainActivity.this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                updateSharedPrefrencesWithLocation(latitude, longitude);
                // \n is for new line
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(this, MainActivity.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                        updateSharedPrefrencesWithLocation(latitude, longitude);
                        // \n is for new line
//                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

//                    Toast.makeText(this, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void updateSharedPrefrencesWithLocation(double latitude, double longitude) {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(String.valueOf(R.string.userSP_location_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        LocationModel locationModel = new LocationModel();
        locationModel.setLatitude(latitude);
        locationModel.setLongitude(longitude);
        float[] results = new float[1];
        Location.distanceBetween(latitude, longitude, latitude, longitude, results);
        try {
            editor.putString(String.valueOf(R.string.userSP_location_key), new ObjectMapper().writeValueAsString(locationModel));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        editor.apply();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity();
            }
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        mHandler.postDelayed(mRunnable, 2000);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

    }
}
