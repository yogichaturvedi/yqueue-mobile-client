package in.yqueue.com.yqueue.api;

import android.os.AsyncTask;


import org.springframework.http.HttpHeaders;

import java.util.concurrent.ExecutionException;

import in.yqueue.com.yqueue.exceptions.NoInternetException;

/**
 * Created by Yogesh on 26-07-2016.
 */
public class HttpGetHeadersService {
    public HttpHeaders getHeaders(String url) {
        try {
            return new HttpGetHeaders(url).execute().get();
        } catch (InterruptedException e) {
            throw new NoInternetException();
        } catch (ExecutionException e) {
            throw new NoInternetException();
        }
    }

    public class HttpGetHeaders extends AsyncTask<Void, Void, HttpHeaders> {
        private String url;

        HttpGetHeaders(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(HttpHeaders t) {
            super.onPostExecute(t);
        }

        @Override
        protected HttpHeaders doInBackground(Void... params) {
            return RestService.instance().getHeaders(url);
        }
    }

}
