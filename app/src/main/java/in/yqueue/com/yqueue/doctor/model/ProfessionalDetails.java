package in.yqueue.com.yqueue.doctor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh Chaturvedi on 23-04-2018.
 */

class ProfessionalDetails {
    private String id;
    private String registrationNum;
    private String qualification;
    private String registrationState;
    private float experience;
    private String specialization;
    private String passingYear;
    private String university;

    public ProfessionalDetails(@JsonProperty("id") String id,
                               @JsonProperty("registrationNum") String registrationNum,
                               @JsonProperty("qualification") String qualification,
                               @JsonProperty("registrationState") String registrationState,
                               @JsonProperty("experience") float experience,
                               @JsonProperty("specialization") String specialization,
                               @JsonProperty("passingYear") String passingYear,
                               @JsonProperty("university") String university) {
        this.id = id;
        this.registrationNum = registrationNum;
        this.qualification = qualification;
        this.registrationState = registrationState;
        this.experience = experience;
        this.specialization = specialization;
        this.passingYear = passingYear;
        this.university = university;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegistrationNum() {
        return registrationNum;
    }

    public void setRegistrationNum(String registrationNum) {
        this.registrationNum = registrationNum;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getRegistrationState() {
        return registrationState;
    }

    public void setRegistrationState(String registrationState) {
        this.registrationState = registrationState;
    }

    public float getExperience() {
        return experience;
    }

    public void setExperience(float experience) {
        this.experience = experience;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getPassingYear() {
        return passingYear;
    }

    public void setPassingYear(String passingYear) {
        this.passingYear = passingYear;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }
}
