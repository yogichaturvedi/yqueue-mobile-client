package in.yqueue.com.yqueue;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.yqueue.com.yqueue.doctor.DoctorDetailActivity;
import in.yqueue.com.yqueue.doctor.model.Address;
import in.yqueue.com.yqueue.doctor.model.DoctorBasicDetail;
import in.yqueue.com.yqueue.service.Move;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {

    private List<NotificationModel> notifications;

    public NotificationListAdapter(List<NotificationModel> notifications) {
        this.notifications = notifications;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NotificationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, notifications);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        NotificationModel notification = notifications.get(position);
        viewHolder.title.setText(notification.getTitle());
        viewHolder.status.setText(notification.getStatus());
        viewHolder.description.setText(notification.getDescription());
        viewHolder.assignedTo.setText(notification.getAssignedTo());
        viewHolder.date.setText(notification.getDate());
        if(notification.getStatus().equalsIgnoreCase("pending")){
            viewHolder.status.setTextColor(Color.RED);
        }
        else if(notification.getStatus().equalsIgnoreCase("completed")){
            viewHolder.status.setTextColor(Color.GREEN);
        }
        else{
            viewHolder.status.setTextColor(Color.parseColor("#FFA500"));

        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return notifications.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView description;
        public TextView date;
        public TextView assignedTo;
        public TextView status;


        public ViewHolder(View itemLayoutView, List<NotificationModel> doctorBasicDetails) {
            super(itemLayoutView);
            title = (TextView) itemLayoutView.findViewById(R.id.title);
            description = (TextView) itemLayoutView.findViewById(R.id.description);
            date = (TextView) itemLayoutView.findViewById(R.id.date);
            assignedTo = (TextView) itemLayoutView.findViewById(R.id.assigned_to);
            status = (TextView) itemLayoutView.findViewById(R.id.status);
        }

    }

}
