//package in.yqueue.com.yqueue.api;
//
//import android.content.Context;
//
//import com.okruti.ecart.exceptions.EcartException;
//import com.okruti.ecart.exceptions.NoInternetException;
//import com.okruti.ecart.model.Filter;
//import com.okruti.ecart.model.Product;
//
//import java.util.concurrent.ExecutionException;
//
///**
// * Created by Yogesh on 08-07-2016.
// */
//public class ProductService {
//    public Product[] loadProducts(Context context, String url) throws EcartException {
//        Product[] products = null;
//        try {
//            products = new HttpGet<Product[]>(context, url, Product[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//        return products;
//    }
//
//    public Product loadProductDetail(Context context, String url) {
//        try {
//            return new HttpGet<Product>(context, url, Product.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public String productStatusInsideCart(Context context, String url) {
//        try {
//            return new HttpGet<String>(context, url, String.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//
//    }
//
//    public String addToCart(Context context, String url) {
//        try {
//            return new HttpGet<String>(context, url, String.class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public Product[] sortProducts(Context context, String url) {
//        try {
//            return new HttpGet<Product[]>(context, url, Product[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public Filter[] getFilters(Context context, String url) {
//        try {
//            return new HttpGet<Filter[]>(context, url, Filter[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public Filter[] setFilter(Context context, String url, Filter[] filters) {
//        //     return new HttpPost<Filter[]>(context, url, Filter[].class).execute().get();
//        return filters;
//    }
//}
