package in.yqueue.com.yqueue.user.appointments;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.common.DialogBoxFrag;
import in.yqueue.com.yqueue.common.MySnackBar;

/**
 * Created by Naveen Kumawat on 28-03-2018.
 */

public class AppointmentListAdapter extends RecyclerView.Adapter<AppointmentListAdapter.ViewHolder> {
    private ArrayList<UserAppointmentPojo> appointmentList;

    public AppointmentListAdapter(ArrayList<UserAppointmentPojo> appointmentList) {

        this.appointmentList = appointmentList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AppointmentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_appointment_list_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView, appointmentList);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        UserAppointmentPojo appointment = appointmentList.get(position);
        appointment.setDoctorName("Yogesh Kumar Chaturvedi");
        String designation = " Dr. ";
        String label = "Appointment with";
        SpannableStringBuilder doctorNameWithLabel = new SpannableStringBuilder(label + designation + appointment.getDoctorName());
        doctorNameWithLabel.setSpan(new ForegroundColorSpan(Color.parseColor("#000000")), label.length(), doctorNameWithLabel.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        viewHolder.name.setText(doctorNameWithLabel);
        String timing = appointment.getDate() + ", " + appointment.getTime();
        viewHolder.timing.setText(timing);
        viewHolder.clinicName.setText(appointment.getClinic());
        viewHolder.specialization.setText(appointment.getSpecialization());
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return appointmentList.size();
    }

    // inner class to hold a reference to each doctor_list_items of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ArrayList<UserAppointmentPojo> appointmentList;
        public TextView name, timing;
        public TextView specialization;
        public TextView clinicName;

        public ViewHolder(View itemLayoutView, ArrayList<UserAppointmentPojo> appointmentList) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.doctor_name);
            timing = (TextView) itemLayoutView.findViewById(R.id.timing);
            clinicName = (TextView) itemLayoutView.findViewById(R.id.doctor_clinic);
            specialization = (TextView) itemLayoutView.findViewById(R.id.doctor_specialization);
            itemLayoutView.findViewById(R.id.button_cancel).setOnClickListener(this);
            this.appointmentList = appointmentList;
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.button_cancel:
                    Callable<Void> positiveFunction = new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            new MySnackBar(itemView, "Appointment Canceled", "short");
                            Intent intent = new Intent(itemView.getContext(), AppointmentListFragment.class);
                            itemView.getContext().startActivity(intent);
                            return null;
                        }
                    };
                    new DialogBoxFrag(view.getContext(),
                            "Cancel Appointment",
                            "Are you sure want to cancel appointment for " + name.getText().toString() + "?",
                            0,
                            true,
                            "Yes",
                            "No",
                            positiveFunction,
                            null);
                    break;
                default:
                    Intent intent = new Intent(view.getContext(), AppointmentDetailActivity.class);
                    view.getContext().startActivity(intent);
                    break;
            }
        }
    }
}
