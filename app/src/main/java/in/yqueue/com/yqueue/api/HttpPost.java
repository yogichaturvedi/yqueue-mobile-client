package in.yqueue.com.yqueue.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import in.yqueue.com.yqueue.common.MyProgressDialog;

/**
 * Created by Yogesh on 26-07-2016.
 */
class HttpPost<T> extends AsyncTask<Void, Void, T> {
    Object request;
    private ProgressDialog progressDialog;
    private String url;
    private Class<T> responseType;
    private Context context;

    public HttpPost(Context context, ProgressDialog progressDialog, String url, Object request, Class<T> responseType) {
        this.context = context;
        this.progressDialog = progressDialog;
        this.url = url;
        this.request = request;
        this.responseType = responseType;
    }

    @Override
    protected void onPreExecute() {
        new MyProgressDialog().show(progressDialog);
    }

    @Override
    protected void onPostExecute(T t) {
        new MyProgressDialog().hide(progressDialog);
    }

    @Override
    protected T doInBackground(Void... params) {
        return RestService.instance().post(this.url, request, responseType);
    }
}
