//package in.yqueue.com.yqueue.api;
//
//import android.content.Context;
//
//import com.example.yogesh.ecart.R;
//import com.okruti.ecart.exceptions.NoInternetException;
//import com.okruti.ecart.model.Category;
//import com.okruti.ecart.model.HomeData;
//
//import java.util.concurrent.ExecutionException;
//
///**
// * Created by Yogesh on 08-07-2016.
// */
//public class HomeService {
//    public HomeData[] getHomeData(Context context, String keywords) {
//        String url = context.getString(R.string.local) + context.getString(R.string.home) + keywords;
//        try {
//            return new HttpGet<HomeData[]>(context, url, HomeData[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//
//    public Category[] getCategorizedList(Context context, String categoryName) {
//        String url = context.getString(R.string.local) + context.getString(R.string.home) + categoryName + "/categorizedView";
//        try {
//            return new HttpGet<Category[]>(context, url, Category[].class).execute().get();
//        } catch (InterruptedException e) {
//            throw new NoInternetException();
//        } catch (ExecutionException e) {
//            throw new NoInternetException();
//        }
//    }
//}
