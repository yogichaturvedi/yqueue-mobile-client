package in.yqueue.com.yqueue.doctor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import in.yqueue.com.yqueue.LocationModel;
import in.yqueue.com.yqueue.R;
import in.yqueue.com.yqueue.api.Constants;
import in.yqueue.com.yqueue.api.RestService;
import in.yqueue.com.yqueue.common.MyProgressDialog;
import in.yqueue.com.yqueue.common.MySnackBar;
import in.yqueue.com.yqueue.doctor.model.DoctorBasicDetail;
import in.yqueue.com.yqueue.doctor.model.Element;
import in.yqueue.com.yqueue.doctor.model.GoogleMapResponse;
import in.yqueue.com.yqueue.doctor.model.Rows;
import in.yqueue.com.yqueue.util.Utils;

import static in.yqueue.com.yqueue.api.Constants.GOOGLE_MAP_API;

public class DoctorListActivity extends AppCompatActivity {

    private ArrayList<DoctorBasicDetail> doctors = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_list_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getIntent().getBundleExtra("search") != null) {
            String keyword = getIntent().getBundleExtra("search").getString("keyword");
            String type = getIntent().getBundleExtra("search").getString("type");
            toolbar.setTitle(keyword);
            //Fetch DoctorList
            getList(type, keyword);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getList(String type, String keyword) {
        ProgressDialog getDoctorsList = new MyProgressDialog().init(DoctorListActivity.this, "", "", true);
        new GetDoctorList(getDoctorsList, type, keyword).execute();
    }


    class GetDoctorList extends AsyncTask<Void, Void, JSONObject> {
        String url = Constants.API_DOCTOR_SEARCH;
        ProgressDialog progressDialog;
        private final String type;
        private final String keyword;

        public GetDoctorList(ProgressDialog progressDialog, String type, String keyword) {
            this.progressDialog = progressDialog;
            this.type = type;
            this.keyword = keyword;
            url += "/" + type + "?keyword=" + keyword + "&locality=sodala&city=jaipur";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new MyProgressDialog().show(progressDialog);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                    new MyProgressDialog().hide(progressDialog);
                }
                if (!statusCode.trim().equalsIgnoreCase("200")) {
                    Callable callable = new Callable() {
                        @Override
                        public Object call() throws Exception {
                            getList(type, keyword);
                            return null;
                        }
                    };
                    new MySnackBar(findViewById(R.id.doctors_list), Utils.getErrorMessage(statusCode), "indeterminant", Color.YELLOW, "RETRY", callable);
                    return;
                }
                try {
                    DoctorBasicDetail[] doctorsArray = (DoctorBasicDetail[]) response.get("response");
                    doctors = new ArrayList<>(Arrays.asList(doctorsArray));
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(String.valueOf(R.string.userSP_location_key), Context.MODE_PRIVATE);
                    String string = sharedPreferences.getString(String.valueOf(R.string.userSP_location_key), "");
                    double latitude = 0, longitude = 0;
                    if (!string.equalsIgnoreCase("")) {
                        try {
                            LocationModel locationObject = new ObjectMapper().readValue(string, LocationModel.class);
                            latitude = locationObject.getLatitude();
                            longitude = locationObject.getLongitude();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    String source = latitude + "," + longitude;
                    ArrayList<String> destinations = new ArrayList<>();
                    for (DoctorBasicDetail doctor : doctors) {
                        double latitude1 = doctor.getAddress().getLatitude();
                        double longitude1 = doctor.getAddress().getLongitude();
                        destinations.add(String.valueOf(latitude1) + "," + String.valueOf(longitude1));
                    }
                    if (latitude == 0 || longitude == 0) {
                        DoctorListAdapter doctorListAdapter = new DoctorListAdapter(doctors, 0, 0);
                        RecyclerView recyclerView = findViewById(R.id.doctors_list);
                        recyclerView.setLayoutManager(new LinearLayoutManager(DoctorListActivity.this, LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(doctorListAdapter);
                    } else {
                        new GetDistance(source, destinations, progressDialog).execute();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    new MyProgressDialog().hide(progressDialog);
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                DoctorBasicDetail[] doctors = RestService.instance().get(url, DoctorBasicDetail[].class);
                jsonObject.put("StatusCode", "200");
                jsonObject.put("response", doctors);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Error in saving patient", error.getLocalizedMessage());
            }
            return jsonObject;
        }
    }

    class GetDistance extends AsyncTask<Void, Void, JSONObject> {
        private final ProgressDialog progressDialog;
        String url = GOOGLE_MAP_API;

        GetDistance(String source, ArrayList<String> destinations, ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
            ArrayList<String> sources = new ArrayList<>();
            for (int i = 0; i < destinations.size(); i++) {
                sources.add(source);
            }
            url += "?origins=" + TextUtils.join("|", sources);
            url += "&destinations=" + TextUtils.join("|", destinations);
            url += "&key=AIzaSyApR7rxIa1T51t7Iu6641lkX__0wU1hcHk";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            new MyProgressDialog().hide(progressDialog);
            String statusCode = "404";
            if (response.has("StatusCode")) {
                try {
                    statusCode = response.getString("StatusCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!statusCode.trim().equalsIgnoreCase("200")) {
                    return;
                }
                try {
                    List<Element> elements = (List<Element>) response.get("distance");
                    int index = 0;
                    for (Element element : elements) {
                        doctors.get(index).getAddress().setDistance(element.getDistance().getText());
                        doctors.get(index).getAddress().setDuration(element.getDuration().getText());
                        index++;
                    }
                    DoctorListAdapter doctorListAdapter = new DoctorListAdapter(doctors, 123, 123);
                    RecyclerView recyclerView = findViewById(R.id.doctors_list);
                    recyclerView.setLayoutManager(new LinearLayoutManager(DoctorListActivity.this, LinearLayoutManager.VERTICAL, false));
                    recyclerView.setAdapter(doctorListAdapter);
                    new MyProgressDialog().hide(progressDialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                GoogleMapResponse distance = RestService.instance().get(url, GoogleMapResponse.class);
                Rows rows = distance.getRows().get(0);
                List<Element> elements = rows.getElements();
//                TextValue distance1 = element.getDistance();
//                TextValue duration = element.getDuration();
//                String text1 = distance1.getText();
//                String text2 = duration.getText();
                jsonObject.put("StatusCode", "200");
                jsonObject.put("distance", elements);
            } catch (Throwable error) {
                try {
                    return new JSONObject().put("StatusCode", error.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return jsonObject;
        }
    }
}