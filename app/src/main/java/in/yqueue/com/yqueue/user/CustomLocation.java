package in.yqueue.com.yqueue.user;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import in.yqueue.com.yqueue.R;

import static android.content.ContentValues.TAG;

public class CustomLocation extends FragmentActivity {

    private PlaceAutocompleteFragment autocompleteFragment;
    private GeoDataClient geoDataClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_custom_location_location);
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder().setCountry("IN").setTypeFilter(5).build();
        final double latitude = getIntent().getExtras().getDouble("_lat");
        final double longitude = getIntent().getExtras().getDouble("_long");

        geoDataClient = Places.getGeoDataClient(this, null);

        autocompleteFragment.setFilter(autocompleteFilter);
        EditText originEditText = (EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input);
        originEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(originEditText, InputMethodManager.SHOW_IMPLICIT);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {

            @Override
            public void onPlaceSelected(Place place) {
//                 TODO: Get info about the selected place.
                Intent intent = new Intent();
                intent.putExtra("_lat", place.getLatLng().latitude);
                intent.putExtra("_long", place.getLatLng().longitude);
                setResult(90, intent);
                finish();
            }

            @Override
            public void onError(Status status) {
//                 TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }
}
